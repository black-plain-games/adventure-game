﻿using AdventureGame.Core.Components;
using AdventureGame.Core.Primitives;
using AdventureGame.Core.Services;

namespace AdventureGame.Game
{
    public class AlternateScene : Scene
    {
        private readonly IInputService _inputService;
        private readonly ISceneManager _sceneManager;

        public AlternateScene(
            IResourceManager resourceManager,
            IContentService contentService,
            IDiagnosticLogger diagnosticLogger,
            IInputService inputService,
            ISceneManager sceneManager)
            : base(resourceManager, contentService, diagnosticLogger, "alternate-scene")
        {
            _inputService = inputService;
            _sceneManager = sceneManager;
        }

        public override void RegisterEvents()
        {
            _inputService.KeyStatesChanged += OnKeysChanged;
        }

        public override void UnregisterEvents()
        {
            _inputService.KeyStatesChanged -= OnKeysChanged;
        }

        private void OnKeysChanged(object sender, KeyStatesChangedEventArgs args)
        {
            var ySpeed = 0.0f;
            var xSpeed = 0.0f;
            var speed = FunConstants.SPEED * 2;

            if (args.IsKey(Keys.W, KeyState.Down))
            {
                ySpeed -= speed;
            }
            if (args.IsKey(Keys.S, KeyState.Down))
            {
                ySpeed += speed;
            }

            if (args.IsKey(Keys.A, KeyState.Down))
            {
                xSpeed -= speed;
            }
            if (args.IsKey(Keys.D, KeyState.Down))
            {
                xSpeed += speed;
            }

            if (xSpeed != 0.0f || ySpeed != 0.0f)
            {
                var player = _resourceManager.Get<Sprite>("player");

                player.Position.Value.X += (int)xSpeed;
                player.Position.Value.Y += (int)ySpeed;
            }

            if (args.IsKey(Keys.O, KeyState.Down))
            {
                SwitchScenes();
            }
        }

        private void SwitchScenes()
        {
            _sceneManager.PopScene();
        }
    }
}
