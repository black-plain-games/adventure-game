﻿using AdventureGame.Core.Components;
using AdventureGame.Core.Primitives;
using AdventureGame.Core.Services;
using System;

namespace AdventureGame.Game
{
    public class TestLoadable : IJsonSerializable
    {
        public string X { get; set; }
        public string Y { get; set; }

        public TestLoadable(string x, string y)
        {
            X = x;
            Y = y;
        }
    }

    public class TestScene : Scene
    {
        private readonly IInputService _inputService;
        private readonly ISceneManager _sceneManager;
        private readonly Func<AlternateScene> _alternateSceneFactory;

        public TestScene(
            IResourceManager resourceManager,
            IContentService contentService,
            IDiagnosticLogger diagnosticLogger,
            IInputService inputService,
            ISceneManager sceneManager,
            Func<AlternateScene> alternateSceneFactory)
            : base(resourceManager, contentService, diagnosticLogger, "test-scene")
        {
            _inputService = inputService;
            _sceneManager = sceneManager;
            _alternateSceneFactory = alternateSceneFactory;
        }

        #region LoadContent

        public override void LoadContent()
        {
            base.LoadContent();

            var font = _resourceManager.Get<IFont>("font-default");

            _diagnosticLogger.CreateMessage("Current Scene", font, $"{GetHashCode()}");

            _resourceManager.Set("input-service", _inputService);
            _resourceManager.Set("diagnostic-logger", _diagnosticLogger);

            CreatePlayer();

            CreateGrass();

            CreateToggleDebugDisplayButton(font);
        }

        private void CreatePlayer()
        {
            var playerTexture = _resourceManager.Get<ITexture2D>("character-01");

            var animationData = _resourceManager.Get<AnimationData>("character-01-animation-data");

            var player = new Sprite(playerTexture, animationData);
            _resourceManager.Set("player", player);

            player.Position.ValueChanged += OnPlayerPositionChanged;

            player.Position.Value.X = 0;
            player.Position.Value.Y = 0;

            player.Dimensions.Value.X = 64;
            player.Dimensions.Value.Y = 64;

            player.Scale.Value.X = 4f;
            player.Scale.Value.Y = 4f;

            var xSpeedApplier = player.AddComponent<ArithmeticComponent>("xspeed");
            xSpeedApplier.GetInput += () => player.Position.Value.X;
            xSpeedApplier.SetOutput += x => player.Position.Value.X = x;

            var ySpeedApplier = player.AddComponent<ArithmeticComponent>("yspeed");
            ySpeedApplier.GetInput += () => player.Position.Value.Y;
            ySpeedApplier.SetOutput += y => player.Position.Value.Y = y;

            var speedComponent = player.AddValue<IPoint<float>>("speed", new Point<float>());
            speedComponent.ValueChanged += (s, e) =>
            {
                xSpeedApplier.Value = e.CurrentValue.X;
                ySpeedApplier.Value = e.CurrentValue.Y;
            };

            var clicked = player.AddComponent("clicked", new ClickedComponent(_inputService, player.Position.Value, player.Dimensions.Value));
            clicked.Clicked += (s, e) =>
            {
                player.Position.Value.X += _r.Next(-100, 100);
                player.Position.Value.Y += _r.Next(-100, 100);
            };
        }

        private void CreateGrass()
        {
            var grassTexture = _contentService.Load<ITexture2D>("grass-01");

            var grass = new Sprite(grassTexture);
            grass.Priority = int.MaxValue - 1;
            _resourceManager.Set("grass", grass);

            grass.Position.Value.X = 300;
            grass.Position.Value.Y = 300;

            grass.Dimensions.Value.X = 16;
            grass.Dimensions.Value.Y = 16;

            grass.Priority = 5;
        }

        private void CreateToggleDebugDisplayButton(IFont font)
        {
            var description = _contentService.Load<ButtonDescription[]>("button-01-description-2");

            var texture = _contentService.Load<ITexture2D>("button-01");

            var button = new Button(texture, font, description);
            button.RegisterEvents(_inputService);
            button.Content.Content.Value = $"Toggle Debug Display";

            button.Pressed += OnToggleDebugDisplay;

            button.Dimensions.Value.X = 300;
            button.Dimensions.Value.Y = 50;

            button.Position.Value.X = 0;
            button.Position.Value.Y = 800;

            _resourceManager.Set("toggle-debug-display-button", button);
        }

        #endregion

        private void OnToggleDebugDisplay(object sender, EventArgs e)
        {
            _diagnosticLogger.Visible = !_diagnosticLogger.Visible;
        }

        public override void RegisterEvents()
        {
            _inputService.MouseStateChanged += UpdateMouseState;
            _inputService.MousePositionChanged += UpdateMousePosition;
            _inputService.MouseButtonChanged += UpdateMouseButtonChanged;
            _inputService.MouseWheelChanged += UpdateMouseWheelChanged;

            var grass = _resourceManager.Get<Sprite>("grass");
            _inputService.MousePositionChanged += UpdateGrassPosition;

            _inputService.KeyStatesChanged += OnKeysChanged;
        }

        public override void UnregisterEvents()
        {
            _inputService.MouseStateChanged -= UpdateMouseState;
            _inputService.MousePositionChanged -= UpdateMousePosition;
            _inputService.MouseButtonChanged -= UpdateMouseButtonChanged;
            _inputService.MouseWheelChanged -= UpdateMouseWheelChanged;

            _inputService.MousePositionChanged -= UpdateGrassPosition;

            _inputService.KeyStatesChanged -= OnKeysChanged;
        }

        private void SwitchScenes()
        {
            var nextScene = _alternateSceneFactory();
            _sceneManager.PushScene(nextScene);
        }

        private void Exit() => _sceneManager.PopScene();

        #region Handlers

        private void UpdateMouseState(object sender, ValueChangedEventArgs<IMouseState> args)
        {
            _mouseStateChanged++;
            _diagnosticLogger.UpdateMessage("Mouse State Changed", _mouseStateChanged);
            _diagnosticLogger.UpdateMessage("Mouse Position", ",", args.CurrentValue.X, args.CurrentValue.Y);
            _diagnosticLogger.UpdateMessage("Mouse Wheel", args.CurrentValue.WheelValue);
            _diagnosticLogger.UpdateMessage("Mouse Buttons", "|", args.CurrentValue.LeftButton, args.CurrentValue.MiddleButton, args.CurrentValue.RightButton);
        }

        private void UpdateMousePosition(object sender, ValueChangedEventArgs<IMouseState> args)
        {
            _mousePositionChanged++;
            _diagnosticLogger.UpdateMessage("Mouse Position Changed", _mousePositionChanged);
        }

        private void UpdateMouseButtonChanged(object sender, ValueChangedEventArgs<IMouseState> args)
        {
            _mouseButtonChanged++;
            _diagnosticLogger.UpdateMessage("Mouse Button Changed", _mouseButtonChanged);
        }

        private void UpdateMouseWheelChanged(object sender, ValueChangedEventArgs<IMouseState> args)
        {
            _mouseWheelChanged++;
            _diagnosticLogger.UpdateMessage("Mouse Wheel Changed", _mouseWheelChanged);
        }

        private void UpdateGrassPosition(object sender, ValueChangedEventArgs<IMouseState> args)
        {
            var grass = _resourceManager.Get<Sprite>("grass");

            grass.Position.Value.X = args.CurrentValue.X;
            grass.Position.Value.Y = args.CurrentValue.Y;
        }

        private void OnPlayerPositionChanged(object sender, ValueChangedEventArgs<IPoint<float>> args)
        {
            _diagnosticLogger.UpdateMessage("Player", ",", args.CurrentValue.X, args.CurrentValue.Y);
        }

        private void OnKeysChanged(object sender, KeyStatesChangedEventArgs args)
        {
            var player = _resourceManager.Get<Sprite>("player");
            var speed = player.GetComponent<IValueComponent<IPoint<float>>>("speed");

            if (args.IsKey(Keys.W, KeyState.Down))
            {
                speed.Value.Y = -FunConstants.SPEED;
            }
            else if (args.IsKey(Keys.S, KeyState.Down))
            {
                speed.Value.Y = FunConstants.SPEED;
            }
            else
            {
                speed.Value.Y = 0;
            }

            if (args.IsKey(Keys.A, KeyState.Down))
            {
                speed.Value.X = -FunConstants.SPEED;
            }
            else if (args.IsKey(Keys.D, KeyState.Down))
            {
                speed.Value.X = FunConstants.SPEED;
            }
            else
            {
                speed.Value.X = 0;
            }

            if (args.IsKey(Keys.P, KeyState.Pressed))
            {
                SwitchScenes();
            }

            if (args.IsKey(Keys.Escape, KeyState.Pressed))
            {
                Exit();
            }
        }

        #endregion

        private int _mouseStateChanged = 0;
        private int _mousePositionChanged = 0;
        private int _mouseButtonChanged = 0;
        private int _mouseWheelChanged = 0;

        private static readonly Random _r = new();
    }
}
