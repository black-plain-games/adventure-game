﻿namespace AdventureGame.Game
{
    public static class FunConstants
    {
        public const int WINDOW_WIDTH = 1920;
        public const int WINDOW_HEIGHT = 1080;
        public const int VIEW_PADDING = 25;
        public const float SPEED = 5f;
    }
}
