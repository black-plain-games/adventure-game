﻿using AdventureGame.Core.Components;
using AdventureGame.Core.Primitives;
using AdventureGame.Core.Services;
using System;
using System.Diagnostics;

namespace AdventureGame.Game
{
    public class PrimitiveTestScene : Scene
    {
        private readonly IInputService _inputService;
        private readonly IGraphicsDeviceInterface _graphicsDeviceInterface;
        private readonly IRenderer _renderer;
        private readonly IVertexInfoCache _vertexInfoCache;

        public PrimitiveTestScene(
            IResourceManager resourceManager,
            IContentService contentService,
            IDiagnosticLogger diagnosticLogger,
            IInputService inputService,
            IGraphicsDeviceInterface graphicsDeviceInterface,
            IRenderer renderer,
            IVertexInfoCache vertexInfoCache)
            : base(resourceManager, contentService, diagnosticLogger, "primitive-test-scene")
        {
            _inputService = inputService;
            _graphicsDeviceInterface = graphicsDeviceInterface;
            _renderer = renderer;
            _vertexInfoCache = vertexInfoCache;
        }

        private CameraComponent _camera;

        public override void LoadContent()
        {
            // Stuff to do:

            // Allow different 3d objects to choose different effects
            // Make 3D movement component
            // Load models
            // 3D lighting
            // Make an orthographic view that we can use to render geometry
            // Migrate spritebatch to ortho view
            // Allow different 3d objects to choose rasterizer state

            _camera = new CameraComponent();
            _camera.Position.Value = new Float3(5, 0, 5);
            _camera.Target.Value = new Float3(1, 0, 1);
            _camera.Mode.Value = CameraTargetMode.Relative;
            _resourceManager.Set("camera", _camera);

            _resourceManager.Set("input", _inputService);
            var primitive = new IndexedPrimitive<XVertexPositionTexture>(_vertexInfoCache);

            var texture = _contentService.Load<ITexture2D>("grass-01");
            _resourceManager.Set("texture-grass", texture);
            primitive.Texture = texture;

            var val = 2;
            var vertices = new[]
            {
                new XVertexPositionTexture(-val, -val, -val, 0, 0),
                new XVertexPositionTexture(val, -val, -val, 0, 1),
                new XVertexPositionTexture(-val, val, -val, 1, 0),
                new XVertexPositionTexture(val, val, -val, 1, 1),

                new XVertexPositionTexture(-val, -val, val, 1, 1),
                new XVertexPositionTexture(val, -val, val, 1, 0),
                new XVertexPositionTexture(-val, val, val, 0, 1),
                new XVertexPositionTexture(val, val, val, 0, 0),
            };

            var indices = new short[]
            {
                0, 2, 1,
                1, 2, 3,

                1, 3, 5,
                5, 3, 7,

                5, 7, 4,
                4, 7, 6,

                4, 6, 0,
                0, 6, 2,

                2, 6, 3,
                3, 6, 7,

                4, 0, 5,
                5, 0, 1
            };

            primitive.Initialize(_graphicsDeviceInterface, vertices, indices);

            var renderable = new Render3DComponent();
            renderable.AddComponent("primitive", primitive);
            _resourceManager.Set("3d-object", renderable);

            base.LoadContent();
        }

        public override void RegisterEvents()
        {
            _inputService.KeyStatesChanged += OnKeyStatesChanged;

            base.RegisterEvents();
        }

        private float _cameraRotation = 0;
        private float _cameraRotationSpeed = 0f;

        private float _cameraXSpeed = 0f;
        private float _cameraZSpeed = 0f;

        private void OnKeyStatesChanged(object sender, KeyStatesChangedEventArgs e)
        {
            if (e.IsKey(Keys.A, KeyState.Down))
            {
                _cameraRotationSpeed = -.05f;
            }
            else if (e.IsKey(Keys.D, KeyState.Down))
            {
                _cameraRotationSpeed = .05f;
            }
            else
            {
                _cameraRotationSpeed = 0f;
            }

            if (e.IsKey(Keys.W, KeyState.Down))
            {
                _cameraXSpeed = (float)(Math.Cos(_cameraRotation) * .5);
                _cameraZSpeed = (float)(Math.Sin(_cameraRotation) * .5);
            }
            else if (e.IsKey(Keys.S, KeyState.Down))
            {
                _cameraXSpeed = -(float)(Math.Cos(_cameraRotation) * .5);
                _cameraZSpeed = -(float)(Math.Sin(_cameraRotation) * .5);
            }
            else
            {
                _cameraXSpeed = 0f;
                _cameraZSpeed = 0f;
            }
        }

        public override void Update()
        {
            if (_cameraRotationSpeed != 0f)
            {
                _cameraRotation += _cameraRotationSpeed;
                _camera.Target.Value.X = (float)Math.Cos(_cameraRotation);
                _camera.Target.Value.Z = (float)Math.Sin(_cameraRotation);
                Debug.WriteLine(_cameraRotation);
            }

            if (_cameraXSpeed != 0f)
            {
                _camera.Position.Value.X += _cameraXSpeed;
            }

            if (_cameraZSpeed != 0f)
            {
                _camera.Position.Value.Z += _cameraZSpeed;
            }

            base.Update();
        }
    }
}
