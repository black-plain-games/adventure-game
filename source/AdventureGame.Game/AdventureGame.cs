﻿using AdventureGame.Core.Services;
using System;

namespace AdventureGame.Game
{
    public class AdventureGame : IGameService
    {
        public event EventHandler OnExit;

        public AdventureGame(
            IWindowManagementService windowManagementService,
            IRenderer renderer,
            ISceneManager sceneManager,
            Func<TestScene> testSceneFactory)
        {
            _windowManagementService = windowManagementService;
            _renderer = renderer;
            _sceneManager = sceneManager;
            var firstScene = testSceneFactory();

            _sceneManager.PushScene(firstScene);
        }

        public void Initialize()
        {
            _windowManagementService.SetWindowDimensions(FunConstants.WINDOW_WIDTH, FunConstants.WINDOW_HEIGHT);
        }

        public void LoadContent()
        {
        }

        public void Update()
        {
            if (!_sceneManager.Update())
            {
                OnExit?.Invoke(this, EventArgs.Empty);
            }
        }

        public void Render()
        {
            _renderer.Clear(System.Drawing.Color.CornflowerBlue);

            _sceneManager.Render();
        }

        private readonly IWindowManagementService _windowManagementService;
        private readonly IRenderer _renderer;
        private readonly ISceneManager _sceneManager;
    }
}
