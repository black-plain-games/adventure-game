﻿using AdventureGame.Core.Services;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace AdventureGame.Game
{
    public static class Package
    {
        public static IServiceCollection AddAdventureGameServices(this IServiceCollection services)
        {
            return services
                .AddScoped<IGameService, AdventureGame>()
                .AddScene<TestScene>()
                .AddScene<AlternateScene>()
                .AddScene<ButtonTestScene>()
                .AddScene<PrimitiveTestScene>();
        }

        public static IServiceCollection AddScene<TScene>(this IServiceCollection services)
            where TScene : class, IScene
        {
            return services
                .AddTransient<TScene>()
                .AddSingleton<Func<TScene>>(x => () => x.GetService<TScene>());
        }
    }
}
