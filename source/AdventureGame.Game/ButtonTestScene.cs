﻿using AdventureGame.Core.Components;
using AdventureGame.Core.Primitives;
using AdventureGame.Core.Services;
using System;
using System.Drawing;

namespace AdventureGame.Game
{
    public class ButtonTestScene : Scene
    {
        private readonly IInputService _inputService;

        public ButtonTestScene(
            IResourceManager resourceManager,
            IContentService contentService,
            IDiagnosticLogger diagnosticLogger,
            IInputService inputService)
            : base(resourceManager, contentService, diagnosticLogger, "button-test-scene")
        {
            _inputService = inputService;
        }

        public override void LoadContent()
        {
            _diagnosticLogger.UpdateMessage("Current Scene", $"{GetHashCode()}");

            _resourceManager.Set("diagnostic-logger", _diagnosticLogger);
            _resourceManager.Set("input-service", _inputService);

            var description = _contentService.Load<ButtonDescription[]>("button-01-description-2");

            var texture = _contentService.Load<ITexture2D>("button-01");

            var font = _contentService.Load<IFont>("font-default");

            for (var rowIndex = 0; rowIndex < 5; rowIndex++)
            {
                for (var columnIndex = 0; columnIndex < 5; columnIndex++)
                {
                    var button = new Button(texture, font, description);
                    button.RegisterEvents(_inputService);
                    button.Content.Content.Value = $"Button {rowIndex}-{columnIndex}";

                    button.Pressed += OnButtonPressed;

                    button.Dimensions.Value.X = 120;
                    button.Dimensions.Value.Y = 50;

                    button.Position.Value.X = 250 + (130 * columnIndex);
                    button.Position.Value.Y = 250 + (60 * rowIndex);

                    button.AddValue("id", $"{rowIndex}-{columnIndex}");
                    button.AddValue("click-count", 0);

                    _resourceManager.Set($"button-{rowIndex}-{columnIndex}", button);
                }
            }

            var cursorTexture = _contentService.Load<ITexture2D>("cursor");
            var cursor = new Sprite(cursorTexture);
            cursor.Priority = int.MaxValue;
            _resourceManager.Set("cursor", cursor);

            var text = new TextRenderable(font, "Hello", Color.Red);
            text.Position.X = 600;
            text.Position.Y = 400;
            _resourceManager.Set("random-text", text);
        }

        private void OnButtonPressed(object sender, EventArgs e)
        {
            var button = sender as Button;
            var id = button.GetValue<string>("id");
            var clickCount = button.GetValue<int>("click-count");
            clickCount.Value += 1;
            _diagnosticLogger.UpdateMessage($"Button {id.Value} Click Count", clickCount.Value);
        }

        public override void RegisterEvents()
        {
            _inputService.MousePositionChanged += (s, e) =>
            {
                _diagnosticLogger.UpdateMessage("Mouse Position", $"({e.CurrentValue.X}, {e.CurrentValue.Y})");
                var cursor = _resourceManager.Get<Sprite>("cursor");
                cursor.Position.Value.X = e.CurrentValue.X;
                cursor.Position.Value.Y = e.CurrentValue.Y;
            };
        }
    }
}
