﻿using AdventureGame.Core.Services;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Xna.Framework.Graphics;

namespace AdventureGame.Driver
{
    public static class Package
    {
        public static IServiceCollection AddAdventureGameDriverServices(this IServiceCollection services) =>
            services
                .AddScoped<XnaWindowManagementService, XnaWindowManagementService>()
                .AddScoped<IWindowManagementService>(s => s.GetRequiredService<XnaWindowManagementService>())
                .AddScoped<IGraphicsDeviceManager>(s => s.GetRequiredService<XnaWindowManagementService>())
                .AddScoped<IGraphicsDeviceService>(s => s.GetRequiredService<XnaWindowManagementService>().GraphicsDeviceManager)
                .AddScoped<IContentService, XnaContentService>()
                .AddScoped<IKeyStateProvider, XnaKeyStateProvider>()
                .AddScoped<IMouseStateProvider, XnaMouseStateProvider>()
                .AddScoped<IRenderer, XnaRenderer>()
                .AddScoped<IGraphicsDeviceInterface>(s => s.GetRequiredService<IRenderer>() as XnaRenderer)
                .AddScoped<IDriver, Driver>();
    }
}
