﻿using AdventureGame.Core.Primitives;
using Microsoft.Xna.Framework.Graphics;

namespace AdventureGame.Driver
{
    public class XnaIndexBuffer : IXIndexBuffer
    {
        public IndexBuffer IndexBuffer => _indexBuffer;

        public XnaIndexBuffer(IndexBuffer indexBuffer)
        {
            _indexBuffer = indexBuffer;
        }

        public void SetIndices(short[] indices)
        {
            _indexBuffer.SetData(indices);
        }

        private readonly IndexBuffer _indexBuffer;
    }

}
