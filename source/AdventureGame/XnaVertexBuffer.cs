﻿using AdventureGame.Core.Primitives;
using Microsoft.Xna.Framework.Graphics;

namespace AdventureGame.Driver
{
    public class XnaVertexBuffer : IXVertexBuffer
    {
        public VertexBuffer VertexBuffer => _vertexBuffer;

        public XnaVertexBuffer(VertexBuffer vertexBuffer)
        {
            _vertexBuffer = vertexBuffer;
        }

        public void SetVertices<TVertex>(TVertex[] vertices)
            where TVertex : struct, IXVertex
        {
            _vertexBuffer.SetData(vertices);
        }

        private readonly VertexBuffer _vertexBuffer;
    }
}
