﻿using Microsoft.Xna.Framework.Graphics;

namespace AdventureGame.Driver
{
    public interface IGraphicsDeviceManager
    {
        GraphicsDevice GraphicsDevice { get; }
    }
}
