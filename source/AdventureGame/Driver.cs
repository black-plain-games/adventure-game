﻿using AdventureGame.Core.Services;
using System.Drawing;

namespace AdventureGame.Driver
{
    public class Driver : Microsoft.Xna.Framework.Game, IDriver
    {
        public Driver(
            IWindowManagementService windowManagementService,
            IRenderer renderer,
            IGameService gameService)
        {
            _renderer = renderer;
            _gameService = gameService;
            windowManagementService.Initialize(this);
        }

        protected override void Initialize()
        {
            _renderer.Initialize();

            _gameService.Initialize();

            _gameService.OnExit += (s, e) => Exit();

            base.Initialize();
        }

        public new void Run()
        {
            base.Run();
        }

        protected override void LoadContent()
        {
            _gameService.LoadContent();

            base.LoadContent();
        }

        protected override void Update(Microsoft.Xna.Framework.GameTime gameTime)
        {
            _gameService.Update();

            base.Update(gameTime);
        }

        protected override void Draw(Microsoft.Xna.Framework.GameTime gameTime)
        {
            _renderer.Clear(Color.CornflowerBlue);

            _gameService.Render();

            base.Draw(gameTime);
        }

        private readonly IRenderer _renderer;
        private readonly IGameService _gameService;
    }
}
