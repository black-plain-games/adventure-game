﻿using AdventureGame.Core;
using AdventureGame.Game;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace AdventureGame.Driver
{
    public static class Program
    {
        [STAThread]
        private static void Main()
        {
            new ServiceCollection()
                .AddAdventureGameCoreServices()
                .AddAdventureGameServices()
                .AddAdventureGameDriverServices()
                .RunDriver();
        }
    }
}