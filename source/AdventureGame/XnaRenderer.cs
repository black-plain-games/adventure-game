﻿using AdventureGame.Core.Primitives;
using AdventureGame.Core.Services;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Linq;

namespace AdventureGame.Driver
{
    public class XnaRenderer : IRenderer, IGraphicsDeviceInterface
    {
        public XnaRenderer(IGraphicsDeviceManager graphicsDeviceManager)
        {
            _graphicsDeviceManager = graphicsDeviceManager;
        }

        public void Initialize()
        {
            _spriteBatch = new SpriteBatch(_graphicsDeviceManager.GraphicsDevice);

            var fieldOfView = (float)Math.PI / 2.0f;

            float width = _graphicsDeviceManager.GraphicsDevice.Viewport.Width;
            float height = _graphicsDeviceManager.GraphicsDevice.Viewport.Height;
            var aspectRatio = width / height;

            var nearPlane = float.Epsilon;
            var farPlane = 1000.0f;

            _2dEffect = new BasicEffect(_graphicsDeviceManager.GraphicsDevice)
            {
                VertexColorEnabled = true,
                Projection = Matrix.CreateOrthographicOffCenter(0, _graphicsDeviceManager.GraphicsDevice.Viewport.Width, _graphicsDeviceManager.GraphicsDevice.Viewport.Height, 0, 0, 1)
            };

            _3dEffect = new BasicEffect(_graphicsDeviceManager.GraphicsDevice)
            {
                TextureEnabled = true,
                VertexColorEnabled = false,
                View = Matrix.CreateLookAt(new Vector3(25, 25, 25), new Vector3(0, 0, 0), Vector3.Up),
                Projection = Matrix.CreatePerspectiveFieldOfView(fieldOfView, aspectRatio, nearPlane, farPlane)
            };
        }

        public void Clear(System.Drawing.Color color)
        {
            var xColor = new Color(color.R, color.G, color.B, color.A);
            _graphicsDeviceManager.GraphicsDevice.Clear(xColor);
        }

        public void Begin2D()
        {
            _spriteBatch.Begin(samplerState: SamplerState.PointClamp);

            _isRenderering2d = true;
        }

        public void End2D()
        {
            _spriteBatch.End();

            _isRenderering2d = false;
        }

        public void Render(ITexture2D texture, IRectangle<float> destination, System.Drawing.Color color)
        {
            var xRect = new Rectangle((int)destination.X, (int)destination.Y, (int)destination.Width, (int)destination.Height);
            var xColor = new Color(color.R, color.G, color.B, color.A);
            _spriteBatch.Draw(GetTexture2D(texture), xRect, xColor);
        }

        public void Render(ITexture2D texture, IRectangle<int> source, IRectangle<float> destination, System.Drawing.Color color)
        {
            var sourceRect = new Rectangle(source.X, source.Y, source.Width, source.Height);
            var destRect = new Rectangle((int)destination.X, (int)destination.Y, (int)destination.Width, (int)destination.Height);
            var xColor = new Color(color.R, color.G, color.B, color.A);
            _spriteBatch.Draw(GetTexture2D(texture),  destRect, sourceRect, xColor);
        }

        public void Render(IFont font, string text, System.Numerics.Vector2 position, System.Drawing.Color color)
        {
            var xPosition = new Vector2(position.X, position.Y);
            var xColor = new Color(color.R, color.G, color.B, color.A);
            _spriteBatch.DrawString(GetSpriteFont(font), text, xPosition, xColor);
        }

        public void Begin3D()
        {
            _isRenderering2d = false;

            _graphicsDeviceManager.GraphicsDevice.RasterizerState = _currentRasterizerState;

            _3dEffect.CurrentTechnique.Passes[0].Apply();
        }

        public void End3D()
        {
        }

        public IXVertexBuffer CreateVertexBuffer<TVertex>(int vertexSize, int vertexCount, params IXVertexDescriptor[] descriptors)
            where TVertex : IXVertex
        {
            var declaration = new VertexDeclaration(vertexSize, ToVertexElements(descriptors));
            var buffer = new VertexBuffer(_graphicsDeviceManager.GraphicsDevice, declaration, vertexCount, BufferUsage.None);
            return new XnaVertexBuffer(buffer);
        }

        public void SetVertexBuffer(IXVertexBuffer vertexBuffer)
        {
            _graphicsDeviceManager.GraphicsDevice.SetVertexBuffer((vertexBuffer as XnaVertexBuffer).VertexBuffer);
        }

        public IXIndexBuffer CreateIndexBuffer(byte indexSize, int indexCount)
        {
            var size = indexSize switch
            {
                2 => IndexElementSize.SixteenBits,
                4 => IndexElementSize.ThirtyTwoBits,
                _ => throw new ArgumentException("Only 16 or 32 are allowable values", nameof(indexSize)),
            };

            var indexBuffer = new IndexBuffer(_graphicsDeviceManager.GraphicsDevice, size, indexCount, BufferUsage.None);

            return new XnaIndexBuffer(indexBuffer);
        }

        public void SetIndexBuffer(IXIndexBuffer indexBuffer)
        {
            _graphicsDeviceManager.GraphicsDevice.Indices = (indexBuffer as XnaIndexBuffer).IndexBuffer;
        }

        public void SetWorld(System.Numerics.Matrix4x4 world)
        {
            _3dEffect.World = ToMatrix(world);
        }

        public void SetView(System.Numerics.Matrix4x4 view)
        {
            _3dEffect.View = ToMatrix(view);
        }

        public void SetView(System.Numerics.Vector3 position, System.Numerics.Vector3 target, System.Numerics.Vector3 up)
        {
            var p = new Vector3(position.X, position.Y, position.Z);
            var t = new Vector3(target.X, target.Y, target.Z);
            var u = new Vector3(up.X, up.Y, up.Z);

            _3dEffect.View = Matrix.CreateLookAt(p, t, u);
        }

        public void RenderIndexedPrimitive(XPrimitiveType type, int baseVertex, int startIndex, int primitiveCount)
        {
            _graphicsDeviceManager.GraphicsDevice.DrawIndexedPrimitives((PrimitiveType)type, baseVertex, startIndex, primitiveCount);
        }

        public void SetTexture(ITexture texture)
        {
            if (_isRenderering2d)
            {
                _2dEffect.Texture = GetTexture2D(texture);
            }
            else
            {
                _graphicsDeviceManager.GraphicsDevice.Textures[0] = ((XnaTexture)texture).Texture;
            }
        }

        private static SpriteFont GetSpriteFont(IFont font) => ((XnaFont)font).Font;

        private static Texture2D GetTexture2D(ITexture texture) => ((XnaTexture2D)texture).Texture2D;

        private static VertexElement[] ToVertexElements(params IXVertexDescriptor[] descriptors)
        {
            return descriptors
                .Select(d => new VertexElement(d.Offset, (VertexElementFormat)d.Format, (VertexElementUsage)d.Usage, d.UsageIndex))
                .ToArray();
        }

        private static Matrix ToMatrix(System.Numerics.Matrix4x4 matrix) =>
            new
            (
                matrix.M11,
                matrix.M12,
                matrix.M13,
                matrix.M14,

                matrix.M21,
                matrix.M22,
                matrix.M23,
                matrix.M24,

                matrix.M31,
                matrix.M32,
                matrix.M33,
                matrix.M34,

                matrix.M41,
                matrix.M42,
                matrix.M43,
                matrix.M44
            );

        private SpriteBatch _spriteBatch;

        private RasterizerState _currentRasterizerState = _defaultRasterizerState;

        private BasicEffect _2dEffect;

        private BasicEffect _3dEffect;

        private bool _isRenderering2d;

        private readonly IGraphicsDeviceManager _graphicsDeviceManager;

        private static readonly RasterizerState _defaultRasterizerState = new()
        {
            CullMode = CullMode.CullClockwiseFace
        };
    }

}
