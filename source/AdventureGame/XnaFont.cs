﻿using AdventureGame.Core.Primitives;
using Microsoft.Xna.Framework.Graphics;

namespace AdventureGame.Driver
{
    internal class XnaFont : IFont
    {
        public SpriteFont Font { get; private set; }

        public int LineSpacing => Font.LineSpacing;

        public XnaFont(SpriteFont font)
        {
            Font = font;
        }

        public IPoint<float> MeasureString(string text)
        {
            var result = Font.MeasureString(text);
            return new PointData<float>(result.X, result.Y);
        }
    }
}
