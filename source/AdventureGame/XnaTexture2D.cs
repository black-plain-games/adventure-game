﻿using AdventureGame.Core.Primitives;
using Microsoft.Xna.Framework.Graphics;

namespace AdventureGame.Driver
{
    internal class XnaTexture2D : XnaTexture, ITexture2D
    {
        public Texture2D Texture2D => Texture as Texture2D;

        public int Width => Texture2D?.Width ?? 0;

        public int Height => Texture2D?.Height ?? 0;

        public XnaTexture2D(Texture2D texture)
            : base(texture)
        {
        }
    }
}
