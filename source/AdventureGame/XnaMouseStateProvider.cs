﻿using AdventureGame.Core.Primitives;
using AdventureGame.Core.Services;

namespace AdventureGame.Driver
{
    public class XnaMouseStateProvider : IMouseStateProvider
    {
        public IMouseState GetMouseState()
        {
            var xnaState = Microsoft.Xna.Framework.Input.Mouse.GetState();
            var currentState = new MouseState
            {
                LeftButton = (ButtonState)xnaState.LeftButton,
                RightButton = (ButtonState)xnaState.RightButton,
                MiddleButton = (ButtonState)xnaState.MiddleButton,
                WheelValue = xnaState.ScrollWheelValue,
                X = xnaState.X,
                Y = xnaState.Y
            };

            if (_previousState != null)
            {
                currentState.LeftButton = GetButtonState(_previousState.LeftButton, currentState.LeftButton);
                currentState.RightButton = GetButtonState(_previousState.RightButton, currentState.RightButton);
                currentState.MiddleButton = GetButtonState(_previousState.MiddleButton, currentState.MiddleButton);
            }

            _previousState = currentState;

            return currentState;
        }

        private ButtonState GetButtonState(ButtonState previous, ButtonState current)
        {
            switch (previous)
            {
                case ButtonState.Up:
                    if (current == ButtonState.Down)
                    {
                        return ButtonState.Pressed;
                    }
                    return ButtonState.Up;
                case ButtonState.Down:
                    if (current == ButtonState.Up)
                    {
                        return ButtonState.Released;
                    }
                    return ButtonState.Down;
                case ButtonState.Pressed:
                    if (current == ButtonState.Down)
                    {
                        return ButtonState.Down;
                    }
                    return ButtonState.Released;
                case ButtonState.Released:
                    if (current == ButtonState.Up)
                    {
                        return ButtonState.Up;
                    }
                    return ButtonState.Pressed;
            }

            return current;
        }

        private MouseState _previousState;

        private class MouseState : IMouseState
        {
            public ButtonState LeftButton { get; set; }
            public ButtonState RightButton { get; set; }
            public ButtonState MiddleButton { get; set; }

            public int X { get; set; }
            public int Y { get; set; }

            public int WheelValue { get; set; }
        }
    }
}
