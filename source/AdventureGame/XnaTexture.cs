﻿using AdventureGame.Core.Primitives;
using Microsoft.Xna.Framework.Graphics;

namespace AdventureGame.Driver
{
    internal class XnaTexture : ITexture
    {
        public Texture Texture { get; private set; }

        public XnaTexture(Texture texture)
        {
            Texture = texture;
        }
    }
}
