﻿using AdventureGame.Core.Primitives;
using AdventureGame.Core.Services;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace AdventureGame.Core
{
    public static class Package
    {
        public static IServiceCollection AddAdventureGameCoreServices(this IServiceCollection services)
        {
            return services
                .AddScoped<IDiagnosticLogger, DiagnosticLogger>()
                .AddScoped<ISceneManager, SceneManager>()
                .AddScoped<IResourceManager, ResourceManager>()
                .AddScoped<IInputService, InputService>()
                .AddScoped<Func<IFont>>(x => () => x.GetService<IContentService>().Load<IFont>("font-default"))
                .AddSingleton<DescribeVertex>(() => (typeof(XVertexPositionColor), XVertexPositionColor.Size, XVertexPositionColor.Descriptors))
                .AddSingleton<DescribeVertex>(() => (typeof(XVertexPositionTexture), XVertexPositionTexture.Size, XVertexPositionTexture.Descriptors))
                .AddScoped<IVertexInfoCache, VertexInfoCache>()
                ;
        }

        public static void RunDriver(this IServiceCollection services)
        {
            using (var provider = services.BuildServiceProvider())
            {
                provider.GetRequiredService<IDriver>().Run();
            }
        }
    }
}
