﻿using AdventureGame.Core.Primitives;
using AdventureGame.Core.Services;
using System;

namespace AdventureGame.Core.Components
{
    public class ClickedComponent : IComponent
    {
        public event EventHandler<IMouseState> Clicked;

        public ClickedComponent(
            IInputService inputService,
            IPoint<float> position,
            IPoint<int> dimensions)
        {
            inputService.MouseButtonChanged += OnMouseStateChanged;
            _position = position;
            _dimensions = dimensions;
        }

        private void OnMouseStateChanged(object sender, ValueChangedEventArgs<IMouseState> e)
        {
            if (Clicked == null)
            {
                return;
            }

            if (e.CurrentValue.LeftButton != ButtonState.Pressed &&
                e.CurrentValue.MiddleButton != ButtonState.Pressed &&
                e.CurrentValue.RightButton != ButtonState.Pressed)
            {
                return;
            }

            if (_position.X <= e.CurrentValue.X &&
                _position.Y <= e.CurrentValue.Y &&
                e.CurrentValue.X <= (_position.X + _dimensions.X) &&
                e.CurrentValue.Y <= (_position.Y + _dimensions.Y))
            {
                Clicked(this, e.CurrentValue);
            }
        }

        private readonly IPoint<float> _position;
        private readonly IPoint<int> _dimensions;
    }
}
