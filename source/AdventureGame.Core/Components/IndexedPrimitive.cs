﻿using AdventureGame.Core.Primitives;
using AdventureGame.Core.Services;
using System;

namespace AdventureGame.Core.Components
{
    public class IndexedPrimitive<TVertex> : I3DRenderable, IComponent
        where TVertex : struct, IXVertex
    {
        public int Priority { get; set; }

        public ITexture Texture { get; set; }

        public bool Visible { get; set; } = true;

        public IndexedPrimitive(int vertexSize)
        {
            _vertexSize = vertexSize;
        }

        public IndexedPrimitive(IVertexInfoCache vertexInfoCache)
        {
            _vertexSize = vertexInfoCache.GetVertexSize<TVertex>();
            _vertexDescriptors = vertexInfoCache.GetVertexDescriptors<TVertex>();
        }

        public void SetVertices(TVertex[] vertices) => _vertexBuffer.SetVertices(vertices);

        public void SetIndices(short[] indices)
        {
            if (indices.Length % 3 != 0)
            {
                throw new ArgumentException("Triangle lists must have 3 indices per triangle", nameof(indices));
            }

            _indexBuffer.SetIndices(indices);

            _triangleCount = indices.Length / 3;
        }

        public void Initialize(IGraphicsDeviceInterface graphicsDeviceInterface, IXVertexDescriptor[] descriptors, int vertexCount, int indexCount)
        {
            _vertexBuffer = graphicsDeviceInterface.CreateVertexBuffer<TVertex>(_vertexSize, vertexCount, descriptors);

            _indexBuffer = graphicsDeviceInterface.CreateIndexBuffer(sizeof(short), indexCount);
        }

        public void Initialize(IGraphicsDeviceInterface graphicsDeviceInterface, IXVertexDescriptor[] descriptors, TVertex[] vertices, short[] indices)
        {
            _vertexBuffer = graphicsDeviceInterface.CreateVertexBuffer<TVertex>(_vertexSize, vertices.Length, descriptors);
            _indexBuffer = graphicsDeviceInterface.CreateIndexBuffer(sizeof(short), indices.Length);
            SetVertices(vertices);
            SetIndices(indices);
        }

        public void Initialize(IGraphicsDeviceInterface graphicsDeviceInterface, TVertex[] vertices, short[] indices)
        {
            _vertexBuffer = graphicsDeviceInterface.CreateVertexBuffer<TVertex>(_vertexSize, vertices.Length, _vertexDescriptors);
            _indexBuffer = graphicsDeviceInterface.CreateIndexBuffer(sizeof(short), indices.Length);
            SetVertices(vertices);
            SetIndices(indices);
        }

        public void Render(IRenderer renderer)
        {
            if (!Visible)
            {
                return;
            }

            renderer.SetTexture(Texture);
            renderer.SetVertexBuffer(_vertexBuffer);
            renderer.SetIndexBuffer(_indexBuffer);
            renderer.RenderIndexedPrimitive(XPrimitiveType.TriangleList, 0, 0, _triangleCount);
        }

        private int _triangleCount;

        private IXVertexBuffer _vertexBuffer;
        private IXIndexBuffer _indexBuffer;

        private readonly int _vertexSize;
        private readonly IXVertexDescriptor[] _vertexDescriptors;
    }
}
