﻿using AdventureGame.Core.Primitives;
using System;

namespace AdventureGame.Core.Components
{
    public interface IAnimationData : IJsonSerializable
    {
        int ColumnCount { get; }
        int FrameCount { get; }
        int SourceOffsetX { get; }
        int SourceOffsetY { get; }
        int SourceMarginX { get; }
        int SourceMarginY { get; }
        int FrameWidth { get; }
        int FrameHeight { get; }
        int[] FrameDelays { get; }

    }
    public class AnimationData : IAnimationData
    {
        public int ColumnCount { get; set; }
        public int FrameCount { get; set; }
        public int SourceOffsetX { get; set; }
        public int SourceOffsetY { get; set; }
        public int SourceMarginX { get; set; }
        public int SourceMarginY { get; set; }
        public int FrameWidth { get; set; }
        public int FrameHeight { get; set; }
        public int[] FrameDelays { get; set; }
    }

    public class FrameTimerComponent : IComponent, IUpdateable
    {
        public event EventHandler Animate;

        public FrameTimerComponent(int[] frameDelays)
        {
            _frameDelays = frameDelays;
            _currentFrameDelay = 0;
            _nextFrameTime = DateTime.Now.AddMilliseconds(_frameDelays[0]);
        }

        public void Update()
        {
            if (DateTime.Now >= _nextFrameTime)
            {
                Animate?.Invoke(this, EventArgs.Empty);
                _currentFrameDelay++;
                if (_currentFrameDelay >= _frameDelays.Length)
                {
                    _currentFrameDelay = 0;
                }
                _nextFrameTime = DateTime.Now.AddMilliseconds(_frameDelays[_currentFrameDelay]);
            }
        }

        private DateTime _nextFrameTime;
        private int[] _frameDelays;
        private int _currentFrameDelay;
    }

    public class Sprite : CompositeComponent
    {
        public override int Priority
        {
            get => GetValue<int>("zindex").Value;
            set => GetValue<int>("zindex").Value = value;
        }

        public IValueComponent<IPoint<float>> Position => GetValue<IPoint<float>>("position");
        public IValueComponent<IPoint<int>> Dimensions => GetValue<IPoint<int>>("dimensions");
        public IValueComponent<IPoint<float>> Scale => GetValue<IPoint<float>>("scale");
        public IValueComponent<ITexture2D> Texture => GetValue<ITexture2D>("texture");

        public Sprite(ITexture2D texture2d)
            : this(texture2d, default)
        {
        }

        public Sprite(ITexture2D texture2d, IAnimationData animationData)
        {
            AddValue("zindex", int.MaxValue);

            var position = AddValue<IPoint<float>>("position", new Point<float>());

            var dimensions = AddValue<IPoint<int>>("dimensions", new Point<int>());

            var scale = AddValue<IPoint<float>>("scale", new Point<float>(1, 1));

            var source = AddValue<IPoint<int>>("source", new Point<int>());
            var sourceRect = AddValue<IRectangle<int>>("source-rect", new Rectangle<int>());

            var destinationRect = AddValue<IRectangle<float>>("destination-rect", new Rectangle<float>());

            var texture = AddValue("texture", texture2d);
            
            var render2dComponent = new Render2DComponent(texture, sourceRect, destinationRect);
            AddComponent("renderable", render2dComponent);

            position.ValueChanged += OnPositionChanged;
            dimensions.ValueChanged += OnDimensionsChanged;
            scale.ValueChanged += OnScaleChanged;
            source.ValueChanged += OnSourceChanged;

            dimensions.Value.X = texture2d.Width;
            dimensions.Value.Y = texture2d.Height;

            if (animationData != default)
            {
                _animationData = animationData;

                var currentFrame = AddValue("current-frame", -1);

                CurrentFrame.ValueChanged += OnCurrentFrameChanged;

                SetFrame(0);

                SourceRect.Value.Width = _animationData.FrameWidth;
                SourceRect.Value.Height = _animationData.FrameHeight;

                var frameTimer = AddComponent("frame-timer", new FrameTimerComponent(_animationData.FrameDelays));

                frameTimer.Animate += OnAnimate;
            }
        }

        private void OnAnimate(object sender, EventArgs e)
        {
            var currentFrame = CurrentFrame.Value;
            currentFrame++;
            currentFrame %= _animationData.FrameCount;
            CurrentFrame.Value = currentFrame;
        }

        private void OnCurrentFrameChanged(object sender, ValueChangedEventArgs<int> args)
        {
            var row = args.CurrentValue / _animationData.ColumnCount;
            var column = args.CurrentValue % _animationData.ColumnCount;

            Source.Value.X = _animationData.SourceOffsetX + (column * _animationData.FrameWidth) + _animationData.SourceMarginX;
            Source.Value.Y = _animationData.SourceOffsetY + (row * _animationData.FrameHeight) + _animationData.SourceMarginY;
        }

        private void SetFrame(int frame)
        {
            CurrentFrame.Value = frame;
        }

        private void OnSourceChanged(object sender, ValueChangedEventArgs<IPoint<int>> args)
        {
            SourceRect.Value.X = args.CurrentValue.X;
            SourceRect.Value.Y = args.CurrentValue.Y;
        }

        private void OnPositionChanged(object sender, ValueChangedEventArgs<IPoint<float>> args)
        {
            DestinationRect.Value.X += args.CurrentValue.X - args.PreviousValue.X;
            DestinationRect.Value.Y += args.CurrentValue.Y - args.PreviousValue.Y;
        }

        private void OnDimensionsChanged(object sender, ValueChangedEventArgs<IPoint<int>> args)
        {
            DestinationRect.Value.Width = args.CurrentValue.X * Scale.Value.X;
            DestinationRect.Value.Height = args.CurrentValue.Y * Scale.Value.Y;
        }

        private void OnScaleChanged(object sender, ValueChangedEventArgs<IPoint<float>> args)
        {
            DestinationRect.Value.Width = args.CurrentValue.X * Dimensions.Value.X;
            DestinationRect.Value.Height = args.CurrentValue.Y * Dimensions.Value.Y;
        }

        private IValueComponent<IRectangle<float>> DestinationRect => GetValue<IRectangle<float>>("destination-rect");

        private IValueComponent<IPoint<int>> Source => GetValue<IPoint<int>>("source");
        private IValueComponent<IRectangle<int>> SourceRect => GetValue<IRectangle<int>>("source-rect");
        private IValueComponent<int> CurrentFrame => GetValue<int>("current-frame");

        private readonly IAnimationData _animationData;
    }
}
