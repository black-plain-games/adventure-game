﻿using AdventureGame.Core.Primitives;
using AdventureGame.Core.Services;
using System.Numerics;

namespace AdventureGame.Core.Components
{
    public class WorldComponent : IComponent, I3DRenderable
    {
        public int Priority { get; set; }

        public Matrix4x4 World { get; set; } = Matrix4x4.Identity;

        public bool Visible { get; set; } = true;

        public void Render(IRenderer renderer)
        {
            if (!Visible)
            {
                return;
            }

            renderer.SetWorld(World);
        }
    }
}
