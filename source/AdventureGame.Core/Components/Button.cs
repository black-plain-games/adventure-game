﻿using AdventureGame.Core.Primitives;
using AdventureGame.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventureGame.Core.Components
{
    public class Button : CompositeComponent
    {
        public override int Priority
        {
            get => GetValue<int>("zindex").Value;
            set => GetValue<int>("zindex").Value = value;
        }

        public IValueComponent<IPoint<float>> Position => GetValue<IPoint<float>>("position");
        public IValueComponent<IPoint<int>> Dimensions => GetValue<IPoint<int>>("dimensions");
        public TextRenderable Content => GetComponent<TextRenderable>("content");

        public event EventHandler Pressed;
        public event EventHandler Released;
        public event EventHandler MouseEntered;
        public event EventHandler MouseLeave;

        public Button(ITexture2D texture2d, IFont font, ButtonDescription[] descriptions)
        {
            AddValue("zindex", 0);
            var texture = AddValue("texture", texture2d);
            AddComponent("content", new TextRenderable(font));
            _descriptions = descriptions.ToDictionary(d => d.Label, d => d);

            AddValue<IPoint<float>>("position", new Point<float>());
            AddValue<IPoint<int>>("dimensions", new Point<int>());
            var contentOffset = AddValue<IPoint<float>>("content-offset", new Point<float>());
            contentOffset.ValueChanged += OnContentOffsetChanged;

            _currentDescriptionLabel = "default";

            foreach (var description in _descriptions)
            {
                CreateVisuals(texture, description.Value);
            }

            Dimensions.ValueChanged += OnDimensionsChanged;
            Position.ValueChanged += OnPositionChanged;
            Content.Dimensions.ValueChanged += OnContentDimensionsChanged;
        }

        private void OnContentOffsetChanged(object sender, ValueChangedEventArgs<IPoint<float>> e)
        {
            UpdateContentPosition(e.CurrentValue);
        }

        private void UpdateContentPosition(IPoint<float> offset)
        {
            Content.Position.X = Position.Value.X + offset.X;
            Content.Position.Y = Position.Value.Y + offset.Y;
        }

        private void OnContentDimensionsChanged(object sender, ValueChangedEventArgs<IPoint<int>> e) => UpdateContentOffset();

        private void OnDimensionsChanged(object sender, ValueChangedEventArgs<IPoint<int>> e)
        {
            UpdateContentOffset();

            foreach (var description in _descriptions)
            {
                UpdateVisualLayout(description.Value, e);
            }
        }

        private void UpdateContentOffset()
        {
            if (Dimensions.Value.X <= 0 || Dimensions.Value.Y <= 0)
            {
                return;
            }

            ContentOffset.Value.X = (Dimensions.Value.X - Content.Dimensions.X) / 2;
            ContentOffset.Value.Y = (Dimensions.Value.Y - Content.Dimensions.Y) / 2;
        }

        private void OnPositionChanged(object sender, ValueChangedEventArgs<IPoint<float>> e)
        {
            UpdateContentPosition(ContentOffset.Value);
        }

        private void CreateVisuals(IValueComponent<ITexture2D> texture, ButtonDescription description)
        {
            var sourceXOffset = 0;
            for (var columnIndex = 0; columnIndex < 3; columnIndex++)
            {
                var sourceYOffset = 0;
                for (var rowIndex = 0; rowIndex < 3; rowIndex++)
                {
                    AddVisualComponent(description, columnIndex, rowIndex, sourceXOffset, sourceYOffset, texture);
                    sourceYOffset += description.RowHeights[rowIndex];
                }
                sourceXOffset += description.ColumnWidths[columnIndex];
            }
        }

        public void RegisterEvents(IInputService inputService)
        {
            inputService.MouseButtonChanged += OnMouseEvent;
            inputService.MousePositionChanged += OnMousePositionChanged;
            MouseEntered += OnHovered;
            MouseLeave += OnHoverEnded;
            Pressed += OnPressed;
            Released += OnReleased;
        }

        private void OnMouseEvent(object sender, ValueChangedEventArgs<IMouseState> e)
        {
            switch (e.CurrentValue.LeftButton)
            {
                case ButtonState.Pressed:
                    if (!IsMouseWithin(e.CurrentValue))
                    {
                        return;
                    }
                    Pressed?.Invoke(this, EventArgs.Empty);
                    break;

                case ButtonState.Released:
                    if (!IsMouseWithin(e.CurrentValue))
                    {
                        return;
                    }
                    Released?.Invoke(this, EventArgs.Empty);
                    break;
            }
        }

        private void OnMousePositionChanged(object sender, ValueChangedEventArgs<IMouseState> e)
        {
            var wasMouseIn = IsMouseWithin(e.PreviousValue);
            var isMouseIn = IsMouseWithin(e.CurrentValue);

            if (isMouseIn && !wasMouseIn)
            {
                MouseEntered?.Invoke(this, EventArgs.Empty);
            }
            else if (wasMouseIn && !isMouseIn)
            {
                MouseLeave?.Invoke(this, EventArgs.Empty);
            }
        }

        private void OnReleased(object sender, EventArgs e)
        {
            ContentOffset.Value.X -= 1;
            ContentOffset.Value.Y -= 1;
            ChangeVisualTo("hovered");
        }

        private void OnPressed(object sender, EventArgs e)
        {
            ContentOffset.Value.X += 1;
            ContentOffset.Value.Y += 1;
            ChangeVisualTo("pressed");
        }

        private void OnHovered(object sender, EventArgs e) => ChangeVisualTo("hovered");

        private void OnHoverEnded(object sender, EventArgs e) => ChangeVisualTo("default");

        private void ChangeVisualTo(string label)
        {
            if (!_descriptions.ContainsKey(label) ||
                _currentDescriptionLabel == label)
            {
                return;
            }

            ChangeIsVisible(_currentDescriptionLabel, false);

            _currentDescriptionLabel = label;

            ChangeIsVisible(_currentDescriptionLabel, true);
        }

        private void ChangeIsVisible(string descriptionLabel, bool isVisible)
        {
            var currentDescription = _descriptions[descriptionLabel];

            for (var columnIndex = 0; columnIndex < currentDescription.ColumnWidths.Length; columnIndex++)
            {
                for (var rowIndex = 0; rowIndex < currentDescription.RowHeights.Length; rowIndex++)
                {
                    var visual = GetComponent<Render2DComponent>(GetComponentKey(_currentDescriptionLabel, "visual", columnIndex, rowIndex));
                    visual.Visible = isVisible;
                }
            }
        }

        private bool IsMouseWithin(IMouseState mouse)
        {
            if (mouse.X < Position.Value.X)
            {
                return false;
            }
            
            if (mouse.Y < Position.Value.Y)
            {
                return false;
            }
            
            if (mouse.X > Position.Value.X + Dimensions.Value.X)
            {
                return false;
            }
            
            if (mouse.Y > Position.Value.Y + Dimensions.Value.Y)
            {
                return false;
            }

            return true;
        }

        private void UpdateVisualLayout(ButtonDescription description, ValueChangedEventArgs<IPoint<int>> e)
        {
            var destinationXOffset = 0;

            for (var columnIndex = 0; columnIndex < 3; columnIndex++)
            {
                var destinationYOffset = 0;

                var columnWidth = description.ColumnWidths[columnIndex];

                if (columnIndex == 1)
                {
                    columnWidth = e.CurrentValue.X - description.ColumnWidths[0] - description.ColumnWidths[2];
                }

                for (var rowIndex = 0; rowIndex < 3; rowIndex++)
                {
                    var rowHeight = description.RowHeights[rowIndex];

                    if (rowIndex == 1)
                    {
                        rowHeight = e.CurrentValue.Y - description.RowHeights[0] - description.RowHeights[2];
                    }

                    var destination = GetValue<IRectangle<float>>(GetComponentKey(description.Label, "destination", columnIndex, rowIndex));

                    destination.Value.X = Position.Value.X + destinationXOffset;
                    destination.Value.Y = Position.Value.Y + destinationYOffset;
                    destination.Value.Width = columnWidth;
                    destination.Value.Height = rowHeight;

                    if (rowIndex == 0)
                    {
                        destinationYOffset += description.RowHeights[rowIndex];
                    }
                    else
                    {
                        destinationYOffset += e.CurrentValue.Y - description.RowHeights[0] - description.RowHeights[2];
                    }
                }

                if (columnIndex == 0)
                {
                    destinationXOffset += description.ColumnWidths[columnIndex];
                }
                else
                {
                    destinationXOffset += e.CurrentValue.X - description.ColumnWidths[0] - description.ColumnWidths[2];
                }
            }
        }

        private void AddVisualComponent(ButtonDescription description, int columnIndex, int rowIndex, int sourceXOffset, int sourceYOffset, IValueComponent<ITexture2D> texture)
        {
            var source = AddValue<IRectangle<int>>(GetComponentKey(description.Label, "source", columnIndex, rowIndex), new Rectangle<int>
                (
                    description.Origin.X + sourceXOffset,
                    description.Origin.Y + sourceYOffset,
                    description.ColumnWidths[columnIndex],
                    description.RowHeights[rowIndex]
                ));

            var destination = AddValue<IRectangle<float>>(GetComponentKey(description.Label, "destination", columnIndex, rowIndex), new Rectangle<float>
                (
                    description.Origin.X,
                    description.Origin.Y,
                    description.ColumnWidths[columnIndex],
                    description.RowHeights[rowIndex]
                ));

            Position.ValueChanged += (s, e) =>
            {
                destination.Value.X += e.CurrentValue.X - e.PreviousValue.X;
                destination.Value.Y += e.CurrentValue.Y - e.PreviousValue.Y;
            };

            AddComponent(GetComponentKey(description.Label, "visual", columnIndex, rowIndex), new Render2DComponent(texture, source, destination)
            {
                Visible = description.Label == _currentDescriptionLabel
            });
        }

        private static string GetComponentKey(string label, string componentType, int columnIndex, int rowIndex) => string.Join("-", label, componentType, columnIndex, rowIndex);

        private IValueComponent<IPoint<float>> ContentOffset => GetValue<IPoint<float>>("content-offset");

        private string _currentDescriptionLabel;
        private readonly IDictionary<string, ButtonDescription> _descriptions;
    }
}
