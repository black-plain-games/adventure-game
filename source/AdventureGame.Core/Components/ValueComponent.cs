﻿using AdventureGame.Core.Primitives;
using System;
using System.Diagnostics;

namespace AdventureGame.Core.Components
{
    [DebuggerDisplay("ValueComponent : {Value}")]
    public class ValueComponent<TValue> : NotifyOnValueChanged<TValue>, IValueComponent<TValue>
    {
        public TValue Value
        {
            get => _value;
            set
            {
                if (IsValueChangeRequestCancelled(_value, value))
                {
                    return;
                }

                if (_value is INotifyOnValueChanged<TValue> previous)
                {
                    previous.ValueChanged -= OnValueChanged;
                }

                var previousValue = _value;
                _value = value;

                if (_value is INotifyOnValueChanged<TValue> next)
                {
                    next.ValueChanged += OnValueChanged;
                }

                OnValueChanged(this, new ValueChangedEventArgs<TValue>(previousValue, _value));
            }
        }

        public ValueComponent()
        {
        }
        
        public ValueComponent(TValue value)
        {
            Value = value;
        }

        private TValue _value;
    }
}
