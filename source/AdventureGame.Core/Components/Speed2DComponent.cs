﻿using AdventureGame.Core.Primitives;
using System;

namespace AdventureGame.Core.Components
{
    public class ArithmeticComponent : IComponent, IUpdateable
    {
        public event Func<float> GetInput;
        public event Action<float> SetOutput;
        public float Value { get; set; }

        public void Update()
        {
            if (SetOutput == null)
            {
                return;
            }

            var input = 0f;
            if (GetInput != null)
            {
                input = GetInput();
            }

            var output = input + Value;

            SetOutput(output);
        }
    }

    public class Speed2DComponent : IComponent, IUpdateable
    {
        public Speed2DComponent(IValueComponent<IPoint<float>> position, IValueComponent<IPoint<float>> speed)
        {
            _position = position;
            _speed = speed;
        }

        public void Update()
        {
            _position.Value.X += _speed.Value.X;
            _position.Value.Y += _speed.Value.Y;
        }

        private readonly IValueComponent<IPoint<float>> _position;
        private readonly IValueComponent<IPoint<float>> _speed;
    }
}
