﻿using AdventureGame.Core.Primitives;

namespace AdventureGame.Core.Components
{
    public interface IValueComponent<TValue> : IValue<TValue>, IComponent, INotifyOnValueChanged<TValue>
    {
    }
}
