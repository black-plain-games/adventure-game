﻿using AdventureGame.Core.Primitives;
using AdventureGame.Core.Services;
using System.Drawing;

namespace AdventureGame.Core.Components
{
    public class Render2DComponent : IComponent, I2DRenderable
    {
        public int Priority { get; set; }

        public bool Visible { get; set; } = true;

        public Render2DComponent(
            IValue<ITexture2D> texture,
            IValue<IRectangle<float>> destination)
        {
            _texture = texture;
            _destination = destination;
        }
        public Render2DComponent(
            IValue<ITexture2D> texture,
            IValue<IRectangle<int>> source,
            IValue<IRectangle<float>> destination)
        {
            _texture = texture;
            _source = source;
            _destination = destination;
        }

        public void Render(IRenderer renderer)
        {
            if (!Visible)
            {
                return;
            }

            if (_source == null)
            {
                renderer.Render(_texture.Value, _destination.Value, Color.White);
            }
            else
            {
                renderer.Render(_texture.Value, _source.Value, _destination.Value, Color.White);
            }
        }

        private readonly IValue<ITexture2D> _texture;
        private readonly IValue<IRectangle<int>> _source;
        private readonly IValue<IRectangle<float>> _destination;
    }
}
