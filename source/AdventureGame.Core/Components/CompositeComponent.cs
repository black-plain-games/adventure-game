﻿using AdventureGame.Core.Primitives;
using AdventureGame.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventureGame.Core.Components
{
    public abstract class CompositeComponent : IComponent, IUpdateable, IRenderable
    {
        public event EventHandler<ComponentChangedEventArgs> ComponentAdded;
        public event EventHandler<ComponentChangedEventArgs> ComponentRemoved;

        public abstract int Priority { get; set; }

        public bool Visible { get; set; } = true;

        protected CompositeComponent()
        {
            _components = new Dictionary<string, IComponent>();
            _updateables = new List<IUpdateable>();
            _renderables = new List<IRenderable>();
        }

        public TComponent AddComponent<TComponent>(string key)
            where TComponent : class, IComponent, new()
        {
            var component = new TComponent();
            AddComponent(key, component);
            return component;
        }

        public TComponent AddComponent<TComponent>(string key, TComponent component)
            where TComponent : IComponent
        {
            _components[key] = component;

            if (component is IUpdateable updateable)
            {
                _updateables.Add(updateable);
            }

            if (component is IRenderable renderable)
            {
                _renderables.Add(renderable);
            }

            ComponentAdded?.Invoke(this, new ComponentChangedEventArgs(key, component));

            return component;
        }

        public IValueComponent<TValue> AddValue<TValue>(string key) => AddValue<TValue>(key, default);

        public IValueComponent<TValue> AddValue<TValue>(string key, TValue value)
        {
            var output = AddComponent<ValueComponent<TValue>>(key);
            output.Value = value;
            return output;
        }

        public IComponent Remove(string key)
        {
            var component = _components[key];

            _components.Remove(key);

            if (component is IUpdateable updateable)
            {
                _updateables.Remove(updateable);
            }

            if (component is IRenderable renderable)
            {
                _renderables.Remove(renderable);
            }

            ComponentRemoved?.Invoke(this, new ComponentChangedEventArgs(key, component));

            return component;
        }

        public void Remove(IComponent component)
        {
            if (!_components.Any(kvp => kvp.Value == component))
            {
                return;
            }

            var kvp = _components.First(kvp => kvp.Value == component);

            Remove(kvp.Key);
        }

        public IValueComponent<TValue> GetValue<TValue>(string key) => GetComponent<IValueComponent<TValue>>(key);

        public TComponent GetComponent<TComponent>(string key)
            where TComponent : class, IComponent
        {
            if (_components.ContainsKey(key))
            {
                return _components[key] as TComponent;
            }

            return default;
        }

        public void Update()
        {
            foreach (var component in _updateables)
            {
                component.Update();
            }
        }

        public void Render(IRenderer renderer)
        {
            if (!Visible)
            {
                return;
            }

            var renderGroups = _renderables.GroupBy(r => r is I2DRenderable);

            foreach (var renderGroup in renderGroups)
            {
                if (renderGroup.Key)
                {
                    renderer.Begin2D();
                }
                else
                {
                    renderer.Begin3D();
                }

                foreach (var renderable in renderGroup.OrderBy(r => r.Priority))
                {
                    renderable.Render(renderer);
                }

                if (renderGroup.Key)
                {
                    renderer.End2D();
                }
                else
                {
                    renderer.End3D();
                }
            }
        }

        private readonly IDictionary<string, IComponent> _components;
        private readonly ICollection<IUpdateable> _updateables;
        private readonly ICollection<IRenderable> _renderables;
    }
}
