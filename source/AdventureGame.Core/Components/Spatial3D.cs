﻿using AdventureGame.Core.Primitives;
using System;
using System.Numerics;

namespace AdventureGame.Core.Components
{
    public class Spatial3D : CompositeComponent, I3DRenderable
    {
        public override int Priority { get; set; }

        public IValueComponent<IFloat3> Scale => GetValue<IFloat3>("scale");
        public IValueComponent<IFloat3> Rotation => GetValue<IFloat3>("rotation");
        public IValueComponent<IFloat3> Position => GetValue<IFloat3>("position");

        public Spatial3D()
            : base()
        {
            var scale = AddValue<IFloat3>("scale", Float3.One);
            var rotation = AddValue<IFloat3>("rotation", Float3.Zero);
            var position = AddValue<IFloat3>("position", Float3.Zero);
            var world = AddComponent<WorldComponent>("world");

            void UpdateWorld(object sender, EventArgs e)
            {
                var newWorld = Matrix4x4.Identity;
                newWorld *= Matrix4x4.CreateScale(scale.Value.X, scale.Value.Y, scale.Value.Z);
                newWorld *= Matrix4x4.CreateRotationX(rotation.Value.X);
                newWorld *= Matrix4x4.CreateRotationY(rotation.Value.Y);
                newWorld *= Matrix4x4.CreateRotationZ(rotation.Value.Z);
                newWorld *= Matrix4x4.CreateTranslation(position.Value.X, position.Value.Y, position.Value.Z);
                world.World = newWorld;
            }

            scale.ValueChanged += UpdateWorld;
            rotation.ValueChanged += UpdateWorld;
            position.ValueChanged += UpdateWorld;
        }
    }
}
