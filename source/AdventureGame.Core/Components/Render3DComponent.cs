﻿using AdventureGame.Core.Primitives;

namespace AdventureGame.Core.Components
{
    public class Render3DComponent : CompositeComponent, I3DRenderable
    {
        public override int Priority { get; set; }

        public IValueComponent<IFloat3> Scale => GetComponent<Spatial3D>("spatial").Scale;
        public IValueComponent<IFloat3> Rotation => GetComponent<Spatial3D>("spatial").Rotation;
        public IValueComponent<IFloat3> Position => GetComponent<Spatial3D>("spatial").Position;

        public Render3DComponent()
            : base()
        {
            AddComponent<Spatial3D>("spatial");
        }
    }
}
