﻿using AdventureGame.Core.Primitives;
using AdventureGame.Core.Services;
using System.Drawing;
using System.Numerics;

namespace AdventureGame.Core.Components
{
    public class TextRenderable : I2DRenderable, IComponent
    {
        public int Priority { get; set; } = int.MaxValue - 1;

        public bool Is3D => false;

        public IValueComponent<IFont> Font { get; } = new ValueComponent<IFont>();

        public IValueComponent<string> Content { get; } = new ValueComponent<string>();

        public Color Color { get; set; }

        public Point<float> Position { get; } = new();

        public Point<int> Dimensions { get; } = new();

        public bool Visible { get; set; } = true;

        public TextRenderable(IFont font)
            : this (font, null)
        {
        }

        public TextRenderable(IFont font, string content)
            : this (font, content, Color.Black)
        {
        }

        public TextRenderable(IFont font, string content, Color color)
        {
            Font.ValueChanged += OnFontChanged;
            Content.ValueChanged += OnContentChanged;
            Position.ValueChanged += UpdatePositionVector;

            Font.Value = font;
            Content.Value = content;
            Color = color;
        }

        private void UpdatePositionVector(object sender, ValueChangedEventArgs<IPoint<float>> e)
        {
            _positionVector.X = e.CurrentValue.X;
            _positionVector.Y = e.CurrentValue.Y;
        }

        private void OnContentChanged(object sender, ValueChangedEventArgs<string> e) => UpdateDimensions();

        private void OnFontChanged(object sender, ValueChangedEventArgs<IFont> e) => UpdateDimensions();

        private void UpdateDimensions()
        {
            if (string.IsNullOrWhiteSpace(Content.Value) ||
                Font.Value == null)
            {
                Dimensions.X = 0;
                Dimensions.Y = 0;
                return;
            }

            var size = Font.Value.MeasureString(Content.Value);

            Dimensions.X = (int)size.X;
            Dimensions.Y = (int)size.Y;
        }
        
        public void Render(IRenderer renderer)
        {
            if (!Visible)
            {
                return;
            }

            if (Font == null || string.IsNullOrWhiteSpace(Content.Value))
            {
                return;
            }

            renderer.Render(Font.Value, Content.Value, _positionVector, Color);
        }

        private Vector2 _positionVector = new();
    }
}
