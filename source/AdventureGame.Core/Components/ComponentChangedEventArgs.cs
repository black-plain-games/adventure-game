﻿using System;

namespace AdventureGame.Core.Components
{
    public class ComponentChangedEventArgs : EventArgs
    {
        public string Key { get; }
        public IComponent Component { get; }

        public ComponentChangedEventArgs(string key, IComponent component)
        {
            Key = key;
            Component = component;
        }
    }
}
