﻿using AdventureGame.Core.Primitives;
using System;
using System.Numerics;

namespace AdventureGame.Core.Components
{
    public enum CameraTargetMode
    {
        /// <summary>
        /// Indicates the camera will treat the target as an absolute position to look at
        /// </summary>
        Absolute,

        /// <summary>
        /// Indicates the camera will treat the target as relative offset from the current position
        /// </summary>
        Relative
    }

    public class CameraComponent : CompositeComponent, I3DRenderable
    {
        public override int Priority { get; set; } = int.MinValue;

        public IValueComponent<IFloat3> Position => GetValue<IFloat3>("position");
        public IValueComponent<IFloat3> Target => GetValue<IFloat3>("target");
        public IValueComponent<IFloat3> Up => GetValue<IFloat3>("up");
        public IValueComponent<CameraTargetMode> Mode => GetValue<CameraTargetMode>("mode"); 

        public CameraComponent()
        {
            var p = AddValue<IFloat3>("position", Float3.Zero);
            var t = AddValue<IFloat3>("target", new Float3(0, 0, 0));
            var u = AddValue<IFloat3>("up", Float3.Up);
            var m = AddValue("mode", CameraTargetMode.Absolute);

            var view = AddComponent<ViewComponent>("view");

            p.ValueChanged += UpdateView;
            t.ValueChanged += UpdateView;
            u.ValueChanged += UpdateView;
            m.ValueChanged += UpdateView;

            void UpdateView(object sender, EventArgs e)
            {
                var pv = p.Value.ToVector3();
                var tv = t.Value.ToVector3();
                var uv = u.Value.ToVector3();

                if (m.Value == CameraTargetMode.Relative)
                {
                    tv += pv;
                }

                view.View = Matrix4x4.CreateLookAt(pv, tv, uv);
            }
        }
    }
}
