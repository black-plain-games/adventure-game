﻿using AdventureGame.Core.Primitives;
using AdventureGame.Core.Services;
using System.Numerics;

namespace AdventureGame.Core.Components
{
    public class ViewComponent : IComponent, I3DRenderable
    {
        public int Priority { get; set; }

        public Matrix4x4 View { get; set; }

        public bool Visible { get; set; } = true;

        public void Render(IRenderer renderer)
        {
            if (!Visible)
            {
                return;
            }

            renderer.SetView(View);
        }
    }
}
