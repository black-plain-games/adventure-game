﻿namespace AdventureGame.Core.Primitives
{
    public interface IVertexInfoCache
    {
        int GetVertexSize<TVertex>()
            where TVertex : IXVertex;

        IXVertexDescriptor[] GetVertexDescriptors<TVertex>()
            where TVertex : IXVertex;
    }
}
