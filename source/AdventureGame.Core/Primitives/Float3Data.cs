﻿namespace AdventureGame.Core.Primitives
{
    public class Float3Data : IFloat3
    {
        public float X { get; set; }
        public float Y { get; set; }
        public float Z { get; set; }

        public Float3Data(float x, float y, float z)
        {
            X = x;
            Y = y;
            Z = z;
        }
    }
}
