﻿namespace AdventureGame.Core.Primitives
{
    public interface IMouseState
    {
        public ButtonState LeftButton { get; }
        public ButtonState RightButton { get; }
        public ButtonState MiddleButton { get; }

        public int X { get; }
        public int Y { get; }

        public int WheelValue { get; }
    }

    public enum ButtonState
    {
        Up,
        Down,
        Pressed,
        Released
    }

    public enum Keys
    {
        Escape,
        W,
        A,
        S,
        D,
        O,
        P,
        Q
    }

    public enum KeyState
    {
        Up,
        Down,
        Pressed,
        Released
    }
}
