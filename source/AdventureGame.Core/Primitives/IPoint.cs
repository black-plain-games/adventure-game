﻿using System.Numerics;

namespace AdventureGame.Core.Primitives
{
    public interface IPoint<TNumber>
    {
        public TNumber X { get; set; }
        public TNumber Y { get; set; }
    }

    public interface IFloat3
    {
        public float X { get; set; }
        public float Y { get; set; }
        public float Z { get; set; }

        public Vector3 ToVector3() => new(X, Y, Z);
    }
}
