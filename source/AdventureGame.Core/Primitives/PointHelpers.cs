﻿namespace AdventureGame.Core.Primitives
{
    public static class PointHelpers
    {
        public static IPoint<float> Add(IPoint<float> a, IPoint<float> b) => new PointData<float>(a.X + b.X, a.Y + b.Y);
        public static IPoint<float> Subtract(IPoint<float> a, IPoint<float> b) => new PointData<float>(a.X - b.X, a.Y - b.Y);
        public static IPoint<float> Multiply(IPoint<float> a, IPoint<float> b) => new PointData<float>(a.X * b.X, a.Y * b.Y);
        public static IPoint<float> Divide(IPoint<float> a, IPoint<float> b) => new PointData<float>(a.X / b.X, a.Y / b.Y);

        public static IPoint<float> Add(IPoint<float> a, float b) => new PointData<float>(a.X + b, a.Y + b);
        public static IPoint<float> Subtract(IPoint<float> a, float b) => new PointData<float>(a.X - b, a.Y - b);
        public static IPoint<float> Multiply(IPoint<float> a, float b) => new PointData<float>(a.X * b, a.Y * b);
        public static IPoint<float> Divide(IPoint<float> a, float b) => new PointData<float>(a.X / b, a.Y / b);
    }
}
