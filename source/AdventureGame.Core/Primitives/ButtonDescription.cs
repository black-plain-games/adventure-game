﻿namespace AdventureGame.Core.Primitives
{
    public class ButtonDescription : IJsonSerializable
    {
        public string Label { get; set; }
        public PointData<int> Origin { get; set; }
        public int[] RowHeights { get; set; }
        public int[] ColumnWidths { get; set; }
    }
}
