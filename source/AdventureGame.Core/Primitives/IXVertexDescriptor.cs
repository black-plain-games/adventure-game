﻿namespace AdventureGame.Core.Primitives
{
    public interface IXVertexDescriptor
    {
        int Offset { get; }
        XVertexDescriptorFormat Format { get; }
        XVertexDescriptorUsage Usage { get; }
        int UsageIndex { get; }
    }
}
