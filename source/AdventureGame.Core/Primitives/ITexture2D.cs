﻿namespace AdventureGame.Core.Primitives
{
    public interface ITexture
    {

    }

    public interface ITexture2D : ITexture
    {
        public int Width { get; }
        public int Height { get; }
    }

    public interface IFont
    {
        int LineSpacing { get; }

        IPoint<float> MeasureString(string text);
    }
}
