﻿using System;

namespace AdventureGame.Core.Primitives
{
    public class ValueChangedEventArgs<T> : EventArgs
    {
        public T PreviousValue { get; }
        public T CurrentValue { get; }

        public ValueChangedEventArgs(T previousValue, T currentValue)
        {
            PreviousValue = previousValue;
            CurrentValue = currentValue;
        }
    }
}
