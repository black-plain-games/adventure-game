﻿using AdventureGame.Core.Services;

namespace AdventureGame.Core.Primitives
{
    public interface IRenderable
    {
        int Priority { get; set; }
        bool Visible { get; set; }
        void Render(IRenderer renderer);
    }
}
