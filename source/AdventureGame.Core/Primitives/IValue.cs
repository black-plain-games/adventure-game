﻿namespace AdventureGame.Core.Primitives
{
    public interface IValue<TValue>
    {
        public TValue Value { get; set; }
    }
}
