﻿using System.Diagnostics;

namespace AdventureGame.Core.Primitives
{
    [DebuggerDisplay("({X},{Y},{Width},{Height})")]
    public class Rectangle<TNumber> : NotifyOnValueChanged<IRectangle<TNumber>>, IRectangle<TNumber>
    {
        public TNumber X
        {
            get => _x;
            set
            {
                if (IsValueChangeRequestCancelled(value, Y, Width, Height))
                {
                    return;
                }
                var previous = _x;
                _x = value;
                OnComponentValueChanged(previous, Y, Width, Height, X, Y, Width, Height);
            }
        }
        public TNumber Y
        {
            get => _y;
            set
            {
                if (IsValueChangeRequestCancelled(X, value, Width, Height))
                {
                    return;
                }
                var previous = _y;
                _y = value;
                OnComponentValueChanged(X, previous, Width, Height, X, Y, Width, Height);
            }
        }
        public TNumber Width
        {
            get => _width;
            set
            {
                if (IsValueChangeRequestCancelled(X, Y, value, Height))
                {
                    return;
                }
                var previous = _width;
                _width = value;
                OnComponentValueChanged(X, Y, previous, Height, X, Y, Width, Height);
            }
        }
        public TNumber Height
        {
            get => _height;
            set
            {
                if (IsValueChangeRequestCancelled(X, Y, Width, value))
                {
                    return;
                }
                var previous = _height;
                _height = value;
                OnComponentValueChanged(X, Y, Width, previous, X, Y, Width, Height);
            }
        }

        public Rectangle()
        {
        }

        public Rectangle(TNumber x, TNumber y, TNumber width, TNumber height)
        {
            X = x;
            Y = y;
            Width = width;
            Height = height;
        }

        public Rectangle(IRectangle<TNumber> other)
            : this(other.X, other.Y, other.Width, other.Height)
        {
        }

        private bool IsValueChangeRequestCancelled(TNumber nextX, TNumber nextY, TNumber nextWidth, TNumber nextHeight)
        {
            var next = new RectangleData<TNumber>(nextX, nextY, nextWidth, nextHeight);
            return IsValueChangeRequestCancelled(this, next);
        }

        private void OnComponentValueChanged(
            TNumber previousX, TNumber previousY, TNumber previousWidth, TNumber previousHeight,
            TNumber currentX, TNumber currentY, TNumber currentWidth, TNumber currentHeight)
        {
            if (IsValueChangedEventNull())
            {
                return;
            }

            var previous = new RectangleData<TNumber>(previousX, previousY, previousWidth, previousHeight);
            var current = new RectangleData<TNumber>(currentX, currentY, currentWidth, currentHeight);
            var newArgs = new ValueChangedEventArgs<IRectangle<TNumber>>(previous, current);
            OnValueChanged(this, newArgs);
        }

        private TNumber _x;
        private TNumber _y;
        private TNumber _width;
        private TNumber _height;
    }
}
