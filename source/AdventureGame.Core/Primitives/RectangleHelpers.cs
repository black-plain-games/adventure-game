﻿using System;

namespace AdventureGame.Core.Primitives
{
    public static class RectangleHelpers
    {
        public static IRectangle<int> GetIntersection(IRectangle<int> lhs, IRectangle<int> rhs)
        {
            var lhsRight = lhs.X + lhs.Width;
            var lhsBottom = lhs.Y + lhs.Height;

            var rhsRight = rhs.X + rhs.Width;
            var rhsBottom = rhs.Y + rhs.Height;


            var left = Math.Clamp(rhs.X, lhs.X, lhsRight);
            var right = Math.Clamp(rhsRight, lhs.X, lhs.X + lhsRight);

            var top = Math.Clamp(rhs.Y, lhs.Y, lhsBottom);
            var bottom = Math.Clamp(rhsBottom, lhs.Y, lhsBottom);

            return new RectangleData<int>(left, top, right - left, bottom - top);
        }

        public static IRectangle<float> GetIntersection(IRectangle<float> lhs, IRectangle<float> rhs)
        {
            var lhsRight = lhs.X + lhs.Width;
            var lhsBottom = lhs.Y + lhs.Height;

            var rhsRight = rhs.X + rhs.Width;
            var rhsBottom = rhs.Y + rhs.Height;


            var left = Math.Clamp(rhs.X, lhs.X, lhsRight);
            var right = Math.Clamp(rhsRight, lhs.X, lhs.X + lhsRight);

            var top = Math.Clamp(rhs.Y, lhs.Y, lhsBottom);
            var bottom = Math.Clamp(rhsBottom, lhs.Y, lhsBottom);

            return new RectangleData<float>(left, top, right - left, bottom - top);
        }
    }
}
