﻿namespace AdventureGame.Core.Primitives
{
    public interface IXVertexBuffer
    {
        void SetVertices<TVertex>(TVertex[] vertices)
            where TVertex : struct, IXVertex;
    }
}
