﻿namespace AdventureGame.Core.Primitives
{
    public class RectangleData<TNumber> : IRectangle<TNumber>
    {
        public TNumber X { get; set; }
        public TNumber Y { get; set; }
        public TNumber Width { get; set; }
        public TNumber Height { get; set; }

        public RectangleData(IRectangle<TNumber> other)
        {
            X = other.X;
            Y = other.Y;
            Width = other.Width;
            Height = other.Height;
        }

        public RectangleData(TNumber x, TNumber y, TNumber width, TNumber height)
        {
            X = x;
            Y = y;
            Width = width;
            Height = height;
        }
    }
}
