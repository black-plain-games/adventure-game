﻿namespace AdventureGame.Core.Primitives
{
    public interface IXIndexBuffer
    {
        void SetIndices(short[] indices);
    }
}
