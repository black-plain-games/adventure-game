﻿using System;

namespace AdventureGame.Core.Primitives
{
    public abstract class NotifyOnValueChanged<TValue> : INotifyOnValueChanged<TValue>
    {
        public event EventHandler<ValueChangeRequestedEventArgs<TValue>> ValueChangeRequested;
        public event EventHandler<ValueChangedEventArgs<TValue>> ValueChanged;

        protected bool IsValueChangedEventNull() => ValueChanged == null;

        protected void OnValueChanged(object sender, ValueChangedEventArgs<TValue> args)
        {
            ValueChanged?.Invoke(sender, args);
        }

        protected bool IsValueChangeRequestCancelled(TValue currentValue, TValue newValue)
        {
            if (ValueChangeRequested == null)
            {
                return false;
            }

            var args = new ValueChangeRequestedEventArgs<TValue>(currentValue, newValue);
            ValueChangeRequested(this, args);
            return args.Cancel;
        }
    }
}
