﻿using System.Diagnostics;
using System.Numerics;

namespace AdventureGame.Core.Primitives
{
    [DebuggerDisplay("{X}, {Y}, {Z}")]
    public class Float3 : NotifyOnValueChanged<IFloat3>, IFloat3
    {
        public static Float3 Zero => new(0, 0, 0);
        public static Float3 One => new(1, 1, 1);
        public static Float3 Up => new(0, 1, 0);

        public float X
        {
            get => _x;
            set
            {
                if (IsValueChangeRequestCancelled(value, Y, Z))
                {
                    return;
                }

                var previous = _x;
                _x = value;
                OnComponentValueChanged(previous, Y, Z, X, Y, Z);
            }
        }

        public float Y
        {
            get => _y;
            set
            {
                if (IsValueChangeRequestCancelled(X, value, Z))
                {
                    return;
                }

                var previous = _y;
                _y = value;
                OnComponentValueChanged(X, previous, Z, X, Y, Z);
            }
        }

        public float Z
        {
            get => _z;
            set
            {
                if (IsValueChangeRequestCancelled(X, Y, value))
                {
                    return;
                }

                var previous = _z;
                _z = value;
                OnComponentValueChanged(X, Y, previous, X, Y, Z);
            }
        }

        public Float3()
            : this(default, default, default)
        {
        }

        public Float3(float x, float y, float z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        private void OnComponentValueChanged(float previousX, float previousY, float previousZ, float currentX, float currentY, float currentZ)
        {
            if (IsValueChangedEventNull())
            {
                return;
            }

            var previous = new Float3Data(previousX, previousY, previousZ);
            var current = new  Float3Data(currentX, currentY, currentZ);
            var newArgs = new ValueChangedEventArgs<IFloat3>(previous, current);
            OnValueChanged(this, newArgs);
        }

        private bool IsValueChangeRequestCancelled(float nextX, float nextY, float nextZ)
        {
            var next = new Float3Data(nextX, nextY, nextZ);
            return IsValueChangeRequestCancelled(this, next);
        }

        private float _x;
        private float _y;
        private float _z;
    }
}
