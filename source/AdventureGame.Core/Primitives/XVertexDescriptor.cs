﻿namespace AdventureGame.Core.Primitives
{
    public class XVertexDescriptor : IXVertexDescriptor
    {
        public int Offset { get; }
        public XVertexDescriptorFormat Format { get; }
        public XVertexDescriptorUsage Usage { get; }
        public int UsageIndex { get; }

        public XVertexDescriptor(int offset, XVertexDescriptorFormat format, XVertexDescriptorUsage usage, int usageIndex)
        {
            Offset = offset;
            Format = format;
            Usage = usage;
            UsageIndex = usageIndex;
        }
    }
}
