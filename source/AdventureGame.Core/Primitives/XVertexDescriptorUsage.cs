﻿namespace AdventureGame.Core.Primitives
{
    public enum XVertexDescriptorUsage
    {
        // Position data
        Position,
        // Color data
        Color,
        // Texture coordinate data or can be used for user-defined data
        TextureCoordinate,
        // Normal data
        Normal
    }
}
