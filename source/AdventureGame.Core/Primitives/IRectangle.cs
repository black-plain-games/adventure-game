﻿namespace AdventureGame.Core.Primitives
{
    public interface IRectangle<TNumber> : IPoint<TNumber>
    {
        public TNumber Width { get; set; }
        public TNumber Height { get; set; }
    }
}
