﻿namespace AdventureGame.Core.Primitives
{
    public enum XPrimitiveType
    {
        TriangleList,
        TriangleStrip,
        LineList,
        LineStrip
    }
}
