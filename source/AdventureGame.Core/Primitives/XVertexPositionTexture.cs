﻿namespace AdventureGame.Core.Primitives
{
    public struct XVertexPositionTexture : IXVertex
    {
        public float X, Y, Z;
        public float U, V;

        public XVertexPositionTexture(float x, float y, float z, float u, float v)
        {
            X = x;
            Y = y;
            Z = z;
            U = u;
            V = v;
        }

        public void SetPosition(float x, float y, float z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public void SetTextureCoordinates(float u, float v)
        {
            U = u;
            V = v;
        }

        private const int PositionSize = sizeof(float) * 3;
        private const int TextureCoordinateSize = sizeof(float) * 2;
        public const int Size = PositionSize + TextureCoordinateSize;

        public static readonly IXVertexDescriptor[] Descriptors = new[]
        {
            new XVertexDescriptor(0, XVertexDescriptorFormat.Vector3, XVertexDescriptorUsage.Position, 0),
            new XVertexDescriptor(PositionSize, XVertexDescriptorFormat.Vector2, XVertexDescriptorUsage.TextureCoordinate, 0),
        };
    }
}
