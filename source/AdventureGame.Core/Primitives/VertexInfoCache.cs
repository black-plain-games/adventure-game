﻿using System;
using System.Collections.Generic;

namespace AdventureGame.Core.Primitives
{
    public delegate (Type, int, IXVertexDescriptor[]) DescribeVertex();

    public class VertexInfoCache : IVertexInfoCache
    {
        public VertexInfoCache(IEnumerable<DescribeVertex> vertexDescriptors)
        {
            foreach (var vertexDescriptor in vertexDescriptors)
            {
                (var type, var size, var descriptors) = vertexDescriptor();
                _vertexTypeInfo[type] = (size, descriptors);
            }
        }

        public int GetVertexSize<TVertex>()
            where TVertex : IXVertex
        {
            return _vertexTypeInfo[typeof(TVertex)].Item1;
        }

        public IXVertexDescriptor[] GetVertexDescriptors<TVertex>()
            where TVertex: IXVertex
        {
            return _vertexTypeInfo[typeof(TVertex)].Item2;
        }

        private readonly IDictionary<Type, (int, IXVertexDescriptor[])> _vertexTypeInfo = new Dictionary<Type, (int, IXVertexDescriptor[])>();
    }
}
