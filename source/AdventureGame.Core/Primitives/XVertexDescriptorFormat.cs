﻿namespace AdventureGame.Core.Primitives
{
    public enum XVertexDescriptorFormat
    {
        // Single 32-bit floating point number.
        Single,
        // Two component 32-bit floating point number.
        Vector2,
        // Three component 32-bit floating point number.
        Vector3,
        // Four component 32-bit floating point number.
        Vector4,
        // Four component, packed unsigned byte, mapped to 0 to 1 range.
        Color,
        // Four component unsigned byte.
        Byte4,
        // Two component signed 16-bit integer.
        Short2,
        // Four component signed 16-bit integer.
        Short4,
        // Normalized, two component signed 16-bit integer.
        NormalizedShort2,
        // Normalized, four component signed 16-bit integer.
        NormalizedShort4,
        // Two component 16-bit floating point number.
        HalfVector2,
        // Four component 16-bit floating point number.
        HalfVector4
    }
}
