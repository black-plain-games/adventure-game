﻿using System;

namespace AdventureGame.Core.Primitives
{
    public class ValueChangeRequestedEventArgs<T> : EventArgs
    {
        public T CurrentValue { get; }
        public T NewValue { get; }
        public bool Cancel { get; set; }

        public ValueChangeRequestedEventArgs(T currentValue, T newValue)
        {
            CurrentValue = currentValue;
            NewValue = newValue;
        }
    }
}
