﻿using System;

namespace AdventureGame.Core.Primitives
{
    public interface INotifyOnValueChanged<TValue>
    {
        public event EventHandler<ValueChangeRequestedEventArgs<TValue>> ValueChangeRequested;
        public event EventHandler<ValueChangedEventArgs<TValue>> ValueChanged;
    }
}
