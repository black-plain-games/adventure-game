﻿using System.Diagnostics;

namespace AdventureGame.Core.Primitives
{
    [DebuggerDisplay("{X}, {Y}")]
    public class Point<TNumber> : NotifyOnValueChanged<IPoint<TNumber>>, IPoint<TNumber>
    {
        public TNumber X
        {
            get => _x;
            set
            {
                if (IsValueChangeRequestCancelled(value, Y))
                {
                    return;
                }

                var previous = _x;
                _x = value;
                OnComponentValueChanged(previous, Y, X, Y);
            }
        }
        public TNumber Y
        {
            get => _y;
            set
            {
                if (IsValueChangeRequestCancelled(X, value))
                {
                    return;
                }

                var previous = _y;
                _y = value;
                OnComponentValueChanged(X, previous, X, Y);
            }
        }

        public Point()
            : this(default, default)
        {
        }

        public Point(TNumber x, TNumber y)
        {
            X = x;
            Y = y;
        }

        private void OnComponentValueChanged(TNumber previousX, TNumber previousY, TNumber currentX, TNumber currentY)
        {
            if (IsValueChangedEventNull())
            {
                return;
            }

            var previous = new PointData<TNumber>(previousX, previousY);
            var current = new PointData<TNumber>(currentX, currentY);
            var newArgs = new ValueChangedEventArgs<IPoint<TNumber>>(previous, current);
            OnValueChanged(this, newArgs);
        }

        private bool IsValueChangeRequestCancelled(TNumber nextX, TNumber nextY)
        {
            var next = new PointData<TNumber>(nextX, nextY);
            return IsValueChangeRequestCancelled(this, next);
        }

        private TNumber _x;
        private TNumber _y;
    }
}
