﻿namespace AdventureGame.Core.Primitives
{
    public struct XVertexPositionColor : IXVertex
    {
        public float X, Y, Z;
        public byte R, G, B, A;

        public XVertexPositionColor(float x, float y, float z, byte r, byte g, byte b, byte a)
        {
            X = x;
            Y = y;
            Z = z;
            R = r;
            G = g;
            B = b;
            A = a;
        }

        public void SetPosition(float x, float y, float z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public void SetColor(byte r, byte g, byte b, byte a)
        {
            R = r;
            G = g;
            B = b;
            A = a;
        }

        public const int Size = (sizeof(float) * 3) + (sizeof(byte) * 4);

        public static readonly IXVertexDescriptor[] Descriptors = new[]
        {
            new XVertexDescriptor(0, XVertexDescriptorFormat.Vector3, XVertexDescriptorUsage.Position, 0),
            new XVertexDescriptor((sizeof(float) * 3) , XVertexDescriptorFormat.Color, XVertexDescriptorUsage.Color, 0),
        };
    }
}
