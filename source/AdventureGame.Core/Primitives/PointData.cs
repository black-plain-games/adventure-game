﻿namespace AdventureGame.Core.Primitives
{
    public class PointData<TNumber> : IPoint<TNumber>
    {
        public TNumber X { get; set; }
        public TNumber Y { get; set; }

        public PointData(TNumber x, TNumber y)
        {
            X = x;
            Y = y;
        }
    }
}
