﻿namespace AdventureGame.Core.Primitives
{
    public interface IUpdateable
    {
        void Update();
    }
}
