﻿using AdventureGame.Core.Components;
using AdventureGame.Core.Primitives;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace AdventureGame.Core.Services
{
    public abstract class Scene : IScene
    {
        public int Priority { get; set; } = int.MaxValue;

        public bool Visible { get; set; } = true;

        public Scene(
            IResourceManager resourceManager,
            IContentService contentService,
            IDiagnosticLogger diagnosticLogger,
            string name)
        {
            _resourceManager = resourceManager;
            _contentService = contentService;
            _diagnosticLogger = diagnosticLogger;
            _name = name;
        }

        public virtual void Update()
        {
            foreach (var updateable in _resourceManager.Get<IUpdateable>())
            {
                updateable.Update();
            }
        }

        public void Render(IRenderer renderer)
        {
            if (!Visible)
            {
                return;
            }

            // Composites first because they're a mix of 2D and 3D

            Render<CompositeComponent>(renderer);

            // 3Ds seconds because they're probably the main content
            renderer.Begin3D();
            Render<I3DRenderable>(renderer);
            renderer.End3D();

            // 2Ds last because they're probably the overlay

            renderer.Begin2D();
            Render<I2DRenderable>(renderer);
            renderer.End2D();

            // NOTE: This might need to be tweaked if we want to blend/optimize 2D/3D rendering
        }

        private void Render<TRenderable>(IRenderer renderer)
            where TRenderable : IRenderable
        {
            var renderables = _resourceManager.Get<TRenderable>(r => r.Visible);

            foreach (var renderable in renderables.OrderBy(r => r.Priority))
            {
                renderable.Render(renderer);
            }
        }

        public virtual void LoadContent()
        {
            _metadata = _contentService.Load<SceneMetadata>($"{_name}-scene-data");

            // Something feels wrong about having the content service
            // do all this resource creation. Maybe we should have the content
            // service be for loading primitive assets (things that go from a file
            // data to an object) and the resource service is for loading complex
            // assets (things that go from file data to objects composed of other objects)

            foreach (var metadata in _metadata.Content)
            {
                var content = _contentService.Load(metadata.Type, metadata.Key);
                _resourceManager.Set(metadata.Key, content);
            }

            foreach (var metadata in _metadata.Resources)
            {
                var parameters = GetParameters(metadata.Parameters);
                var resource = _contentService.Create(metadata.Type, parameters);
                // initialization
                _resourceManager.Set(metadata.Key, resource);
            }
        }

        private object[] GetParameters(JObject parameterMetadata)
        {
            if (parameterMetadata == null)
            {
                return Array.Empty<object>();
            }

            var parameters = new List<object>();

            foreach ((_, var parameterValue) in parameterMetadata)
            {
                object value = null;

                switch (parameterValue.Type)
                {
                    case JTokenType.String:
                        value = ConvertFromString(parameterValue.Value<string>());
                        break;

                    case JTokenType.Boolean:
                        value = parameterValue.Value<bool>();
                        break;

                    case JTokenType.Integer:
                        value = parameterValue.Value<int>();
                        break;
                }

                parameters.Add(value);
            }

            return parameters.ToArray();
        }

        private object ConvertFromString(string value)
        {
            var parameterString = $"{value}";

            var resource = _resourceManager.Get(parameterString);

            return resource ?? parameterString;
        }

        public virtual void UnloadContent()
        {
            foreach (var contentMetadata in _metadata.Content)
            {
                _resourceManager.Remove(contentMetadata.Key);
            }
        }

        public virtual void RegisterEvents() { }
        public virtual void UnregisterEvents() { }

        private SceneMetadata _metadata;

        protected readonly IResourceManager _resourceManager;
        protected readonly IContentService _contentService;
        protected readonly IDiagnosticLogger _diagnosticLogger;
        protected string _name;
    }

    [DebuggerDisplay("{Type} {Key}")]
    public class ContentMetadata : IJsonSerializable
    {
        public string Type { get; set; }
        public string Key { get; set; }
    }

    [DebuggerDisplay("{Type} {Key}")]
    public class ResourceMetadata : IJsonSerializable
    {
        public string Type { get; set; }
        public string Key { get; set; }
        public JObject Parameters { get; set; }
        public JObject Initialization { get; set; }
    }

    public class SceneMetadata : IJsonSerializable
    {
        public IEnumerable<ContentMetadata> Content { get; set; }
        public IEnumerable<ResourceMetadata> Resources { get; set; }
    }
}
