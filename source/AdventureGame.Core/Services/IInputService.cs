﻿using AdventureGame.Core.Primitives;
using System;

namespace AdventureGame.Core.Services
{
    public interface IInputService : IUpdateable
    {
        event EventHandler<KeyStatesChangedEventArgs> KeyStatesChanged;
        public event EventHandler<ValueChangedEventArgs<IMouseState>> MouseStateChanged;
        public event EventHandler<ValueChangedEventArgs<IMouseState>> MousePositionChanged;
        public event EventHandler<ValueChangedEventArgs<IMouseState>> MouseButtonChanged;
        public event EventHandler<ValueChangedEventArgs<IMouseState>> MouseWheelChanged;
    }
}
