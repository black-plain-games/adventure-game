﻿using AdventureGame.Core.Primitives;
using System.Collections.Generic;

namespace AdventureGame.Core.Services
{
    public interface IKeyStateProvider
    {
        public IReadOnlyDictionary<Keys, KeyState> GetKeyStates();
    }
}
