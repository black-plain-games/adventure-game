﻿using AdventureGame.Core.Primitives;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Numerics;

namespace AdventureGame.Core.Services
{
    public class DiagnosticLogger : IDiagnosticLogger, I2DRenderable
    {
        public int Priority { get; set; } = int.MaxValue;

        public bool Visible { get; set; } = true;

        public DiagnosticLogger(Func<IFont> defaultFontFactory)
        {
            _defaultFontFactory = defaultFontFactory;
            _permaPrintMessages = new Dictionary<string, PermaPrintMessage>();
        }

        public void CreateMessage(string key, IFont font, string text = "")
        {
            var message = new PermaPrintMessage(font, Color.Black, text);

            _permaPrintMessages.Add(key, message);
        }

        public void UpdateMessage(string key, string text)
        {
            if (!_permaPrintMessages.ContainsKey(key))
            {
                _permaPrintMessages[key] = new PermaPrintMessage(GetDefaultFont(), Color.Black, text);
            }
            else
            {
                _permaPrintMessages[key].Text = text;
            }
        }

        public void UpdateMessage(string key, object value) => UpdateMessage(key, $"{value}");

        public void UpdateMessage(string key, string delimiter, params object[] values) => UpdateMessage(key, $"({string.Join(delimiter, values)})");

        public void UpdateMessage(string key, Color color)
        {
            _permaPrintMessages[key].Color = color;
        }

        public void UpdateMessage(string key, string text, Color color)
        {
            _permaPrintMessages[key].Text = text;
            _permaPrintMessages[key].Color = color;
        }

        public void Render(IRenderer renderer)
        {
            if (!Visible)
            {
                return;
            }

            var position = new Vector2(0, 0);

            foreach ((var key, var message) in _permaPrintMessages)
            {
                renderer.Render(message.Font, $"{key} : {message.Text}", position, message.Color);

                position.Y += message.Font.LineSpacing;
            }
        }

        private IFont GetDefaultFont() => _defaultFontFactory?.Invoke();

        private class PermaPrintMessage
        {
            public IFont Font { get; }

            public Color Color { get; set; }

            public string Text { get; set; }

            public PermaPrintMessage(IFont font, Color color, string text)
            {
                Font = font;
                Color = color;
                Text = text;
            }
        }

        private readonly Func<IFont> _defaultFontFactory;
        private readonly IDictionary<string, PermaPrintMessage> _permaPrintMessages;
    }
}
