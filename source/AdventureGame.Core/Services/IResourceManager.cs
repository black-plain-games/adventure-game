﻿using System;
using System.Collections.Generic;

namespace AdventureGame.Core.Services
{
    public interface IResourceManager
    {
        IEnumerable<T> Get<T>();
        IEnumerable<T> Get<T>(Predicate<T> condition);
        T Get<T>(string key) where T : class;
        object Get(string key);
        void Remove(object resource);
        object Remove(string key);
        object Set(string key, object resource);
    }
}