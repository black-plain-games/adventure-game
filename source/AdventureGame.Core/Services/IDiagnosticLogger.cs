﻿using AdventureGame.Core.Primitives;
using System.Drawing;

namespace AdventureGame.Core.Services
{
    public interface IDiagnosticLogger : IRenderable
    {
        void CreateMessage(string key, IFont font, string text = "");
        void UpdateMessage(string key, Color color);
        void UpdateMessage(string key, string text);
        void UpdateMessage(string key, object value);
        void UpdateMessage(string key, string delimiter, params object[] values);
        void UpdateMessage(string key, string text, Color color);
    }
}