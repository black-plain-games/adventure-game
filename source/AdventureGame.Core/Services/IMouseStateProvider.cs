﻿using AdventureGame.Core.Primitives;

namespace AdventureGame.Core.Services
{
    public interface IMouseStateProvider
    {
        public IMouseState GetMouseState();
    }
}
