﻿using AdventureGame.Core.Primitives;
using System.Drawing;
using System.Numerics;

namespace AdventureGame.Core.Services
{
    public interface IGraphicsDeviceInterface
    {
        IXVertexBuffer CreateVertexBuffer<TVertex>(int vertexSize, int vertexCount, params IXVertexDescriptor[] descriptors)
            where TVertex : IXVertex;
        void SetVertexBuffer(IXVertexBuffer vertexBuffer);

        IXIndexBuffer CreateIndexBuffer(byte indexSize, int indexCount);
        void SetIndexBuffer(IXIndexBuffer indexBuffer);
    }

    public interface IRenderer
    {
        void Initialize();
        void Clear(Color color);
        void Render(ITexture2D texture, IRectangle<float> destination, Color color);
        void Render(ITexture2D texture, IRectangle<int> source, IRectangle<float> destination, Color color);
        void Render(IFont font, string text, Vector2 position, Color color);
        void SetVertexBuffer(IXVertexBuffer vertexBuffer);
        void SetIndexBuffer(IXIndexBuffer indexBuffer);
        void SetWorld(Matrix4x4 matrix);
        void SetView(Matrix4x4 view);
        void SetView(Vector3 position, Vector3 target, Vector3 up);
        void RenderIndexedPrimitive(XPrimitiveType type, int baseVertex, int startIndex, int primitiveCount);
        void SetTexture(ITexture texture);
        void Begin3D();
        void End3D();
        void Begin2D();
        void End2D();
    }
}
