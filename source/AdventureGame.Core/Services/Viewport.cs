﻿using AdventureGame.Core.Components;
using AdventureGame.Core.Primitives;
using System.Collections.Generic;

namespace AdventureGame.Core.Services
{
    public class Viewport
    {
        public ICollection<IPoint<float>> Targets { get; }

        public IPoint<float> Origin => _origin;

        public Viewport()
        {
            _origin = new Point<float>();
            _origin.ValueChanged += OnOriginChanged;
            Targets = new List<IPoint<float>>();
        }

        public void Add(IPoint<float> target) => Targets.Add(target);

        private void OnOriginChanged(object sender, ValueChangedEventArgs<IPoint<float>> e)
        {
            var delta = PointHelpers.Subtract(e.PreviousValue, e.CurrentValue);

            if (delta.X == 0 && delta.Y == 0)
            {
                return;
            }

            foreach (var target in Targets)
            {
                target.X += delta.X;
                target.Y += delta.Y;
            }
        }

        private readonly Point<float> _origin;
    }
}
