﻿using System;

namespace AdventureGame.Core.Services
{
    public interface IDriver : IDisposable
    {
        void Run();
    }
}
