﻿using AdventureGame.Core.Primitives;

namespace AdventureGame.Core.Services
{
    public interface IScene : IUpdateable, IRenderable
    {
        void LoadContent();
        void RegisterEvents();
        void UnregisterEvents();
        void UnloadContent();
    }
}
