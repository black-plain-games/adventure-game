﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventureGame.Core.Services
{
    public class ResourceManager : IResourceManager
    {
        public ResourceManager()
        {
            _resources = new Dictionary<string, object>();
        }

        public ResourceManager(IDictionary<string, object> resources)
        {
            _resources = new Dictionary<string, object>(resources);
        }

        public object Set(string key, object resource)
        {
            _resources.TryGetValue(key, out var previousResource);

            _resources[key] = resource;

            return previousResource;
        }

        public object Get(string key)
        {
            return _resources.ContainsKey(key) ? _resources[key] : null;
        }

        public T Get<T>(string key)
            where T : class
        {
            return Get(key) as T;
        }

        public IEnumerable<T> Get<T>()
        {
            return _resources.Values.Where(x => x is T).Cast<T>();
        }

        public IEnumerable<T> Get<T>(Predicate<T> condition)
        {
            return _resources.Values.Where(x => x is T).Cast<T>().Where(x => condition(x));
        }

        public object Remove(string key)
        {
            if (_resources.TryGetValue(key, out var result))
            {
                _resources.Remove(key);
            }

            return result;
        }

        public void Remove(object resource)
        {
            string resourceKey = null;

            foreach ((var key, var value) in _resources)
            {
                if (value == resource)
                {
                    resourceKey = key;
                    break;
                }
            }

            if (resourceKey == null)
            {
                return;
            }

            _resources.Remove(resourceKey);
        }

        private readonly IDictionary<string, object> _resources;
    }
}
