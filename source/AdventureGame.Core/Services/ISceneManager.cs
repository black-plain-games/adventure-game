﻿using System;

namespace AdventureGame.Core.Services
{
    public interface ISceneManager
    {
        event EventHandler<SceneChangedEventArgs> SceneChanged;

        void PushScene(IScene scene);
        void PopScene();
        void SwapScene(IScene scene);
        void Render();
        bool Update();
    }
}